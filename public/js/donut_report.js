	 var donutData1 = [
      {label: " " , data: 60, color: "#31ca6a"},
      {label: " ", data: 60, color: "#327aba"},
      {label: " ", data: 30, color: "#fc5d56"},
      {label: " ", data: 30, color: "#e70047"}

    ];
    $("#donut-chart1").css('height','200px');
    $.plot("#donut-chart1", donutData1, {
      series: {
        pie: {
          show: true,
          radius: 1,
          innerRadius: 0.5,
          label: {
            show: true,
            radius: 2 / 3,
             
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    });


    var donutData2 = [
      
      {label: " ", data: 120, color: "#327aba"},
      {label: " ", data: 40, color: "#fc5d56"},
      {label: " ", data: 20, color: "#31ca6a"}

    ];
    $("#donut-chart2").css('height','200px');
    $.plot("#donut-chart2", donutData2, {
      series: {
        pie: {
          show: true,
          radius: 1,
          innerRadius: 0.5,
          label: {
            show: true,
            radius: 2 / 3,
             
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    });