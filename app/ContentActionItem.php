<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentActionItem extends Model
{
    protected $table = 'ActionItemContents';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
