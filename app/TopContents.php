<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopContents extends Model
{
    protected $table = 'TopContent';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
