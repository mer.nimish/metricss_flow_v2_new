<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use DB;
use App\Leads as Leads;
use App\Leads_Information as Leads_Information;

use App\ContentConversion as ContentConversion;



class LeadController extends Controller
{
    public function index(Request $request)
    {
        $client_id=Session('client_id');
        $e_id=$request->e_id;

        return view('LeadID', [
            'client_id'=>$client_id,
            'e_id' => $e_id
        ]);
    }

    public function getLeadData()
    {
        $client_id = Input::get('clientID');
        $days = Input::get('days');
        $e_id=Input::get('e_id');
        $lead_id = Leads::where([['client_id', $client_id],['e_id', $e_id]])->first();
        $lead_info = Leads_Information::where([['client_id', $client_id],['e_id', $e_id]])->first();
        
        $userDetails['UserID']  = $e_id;
        $userDetails['Stage']  = $lead_id->Stage;
        $userDetails['LastSeen']  = $lead_id->last_seen;
        $userDetails['FirstSeen']  = $lead_id->timedate;
        $userDetails['Location']  = $lead_id->country;
        $userDetails['IPAddress']  = $lead_info->ip;
        $userDetails['Language']  =$lead_id->language;
        $userDetails['MostCommonDevice']  = $lead_id->device_type;
        $userDetails['DeviceBrand']  = $lead_id->device_brand;;
        $userDetails['DeviceModel']  = $lead_id->device_model;;
        $userDetails['ToughScreen']  = $lead_id->hastouchscreen;
        $userDetails['OperatingSystem']  = $lead_id->OS_name;
        $userDetails['Browser']  = $lead_id->browser;
        $userDetails['Campaign']  = $client_id;
        $userDetails['Channels']  = $lead_id->domainname;;
        $userDetails['AdBlocker']  = $lead_id->hasAdblock;;
        $userDetails['Flash']  = $lead_id->hasFlash;;
        $userDetails['Html5']  = $lead_id->canvas;;
        $userDetails['Siverlight']  = $lead_id->colorDepth;;

        $behaviorData['TypicalActiveTime']  = $lead_id->timedate;
        $behaviorData['MostCommonDevice']  = $lead_id->device_type;
        $behaviorData['AverageSessionLength']  = sprintf('%02d:%02d:%02d', ($lead_id->totaltime/3600),($lead_id->totaltime/60%60), $lead_id->totaltime%60).' h:m:s';
        $behaviorData['AvgClicksperSession']  = $lead_id->clickcount;

        $lead_daily = Leads::where([['client_id', $client_id],['e_id', $e_id]])->orderBy('Date', 'DESC')->take(4)->get();
        $lead_daily_count=Leads::where([['client_id', $client_id],['e_id', $e_id]])->orderBy('Date', 'DESC')->count();

        if ($lead_daily_count>0){
            $t= $lead_daily[0]->totaltime;
            $dailyActivity['AvgTime1']  = sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60).' h:m:s';
            $dailyActivity['AvgClicks1']  = $lead_daily[0]->clickcount;
        }else{
            $dailyActivity['AvgTime1']  ='00:00:00 h:m:s';
            $dailyActivity['AvgClicks1']  = '0';
        }
        if ($lead_daily_count>1){
            $t= $lead_daily[1]->totaltime;
            $dailyActivity['AvgTime2']  = sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60).' h:m:s';
            $dailyActivity['AvgClicks2']  = $lead_daily[1]->clickcount;
        }else{
            $dailyActivity['AvgTime2']  ='00:00:00 h:m:s';
            $dailyActivity['AvgClicks2']  = '0';   
        }

        if ($lead_daily_count>2){
            $t= $lead_daily[2]->totaltime;
            $dailyActivity['AvgTime3']  =sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60).' h:m:s';
            $dailyActivity['AvgClicks3']  =$lead_daily[2]->clickcount;
        }else{
            $dailyActivity['AvgTime3']  ='00:00:00 h:m:s';
            $dailyActivity['AvgClicks3']  = '0'; 
        }

        if ($lead_daily_count>3){
            $t= $lead_daily[3]->totaltime;
            $dailyActivity['AvgTime4']  =sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60).' h:m:s';
            $dailyActivity['AvgClicks4']  =$lead_daily[3]->clickcount;
        }else{
            $dailyActivity['AvgTime4']  ='00:00:00 h:m:s';
            $dailyActivity['AvgClicks4']  = '0'; 
        }

        $lead_sessions= Leads::where([['client_id', $client_id],['e_id', $e_id]])->count();
        $lead_unknown= Leads::where([['client_id', $client_id],['e_id', $e_id],['Status', 'Unknown']])->count();
        $sessionProfile['TotalSessions']  = $lead_sessions;
        $sessionProfile['SessionDetails'][] = array(
                                            'label' => 'Known Sessons',
                                            'highlight' => '#e70047',
                                            'value' => $lead_sessions-$lead_unknown,
                                            'color' => '#e70047'
                                        );
     
        
        $sessionProfile['SessionDetails'][] = array(
                                            'label' => 'Linked Anonymous Sessions',
                                            'highlight' => '#327aba',
                                            'value' => $lead_unknown,
                                            'color' => '#327aba'
                                        );
        $sessionProfile['PredictionAccuracy']  = round(($lead_sessions-$lead_unknown)/$lead_sessions*100);


        $lead_aware=Leads::where([['e_id', $e_id],['Stage', 'Awareness']])->first();
        $lead_enaged=Leads::where([['e_id', $e_id],['Stage', 'Deciding']])->first();
        $lead_considering=Leads::where([['e_id', $e_id],['Stage', 'Considering']])->first();
        $lead_converted=Leads::where([['e_id', $e_id],['Stage', 'Converted']])->first();
        
        $count_aware=Leads::where([['e_id', $e_id],['Stage', 'Awareness']])->count();
        $count_enaged=Leads::where([['e_id', $e_id],['Stage', 'Deciding']])->count();
        $count_considering=Leads::where([['e_id', $e_id],['Stage', 'Considering']])->count();
        $count_converted=Leads::where([['e_id', $e_id],['Stage', 'Converted']])->count();
        
        if ($count_aware>0){
            $leadProgression['PageTitle1']  = $lead_aware->PageName;
            $leadProgression['PageURL1']  = $lead_aware->PageURL;
            $leadProgression['PageDate1']  = date("M d, Y",strtotime($lead_aware->Date));
            $leadProgression['PageStage1']  = 'Became<br />Aware';
        }
        else{
            $leadProgression['PageTitle1']  = '';
            $leadProgression['PageURL1']  = '';
            $leadProgression['PageDate1']  = '';
            $leadProgression['PageStage1']  = 'Became<br />Aware';
        }

        if ($count_considering>0){
            $leadProgression['PageTitle2']  = $lead_considering->PageName;
            $leadProgression['PageURL2']  = $lead_considering->PageURL;
            $leadProgression['PageDate2']  = date("M d, Y",strtotime($lead_considering->Date));
            $leadProgression['PageStage2']  = 'Started<br />Considering';
        }else{
            $leadProgression['PageTitle2']  = '';
            $leadProgression['PageURL2']  = '';
            $leadProgression['PageDate2']  = '';
            $leadProgression['PageStage2']  = 'Started<br />Considering';
        }

        if ($count_enaged>0){
            $leadProgression['PageTitle3']  =  $lead_enaged->PageName;
            $leadProgression['PageURL3']  = $lead_enaged->PageURL;
            $leadProgression['PageDate3']  = date("M d, Y",strtotime($lead_enaged->Date));
            $leadProgression['PageStage3']  = 'Started<br />Deciding';
        }else{
            $leadProgression['PageTitle3']  =  '';
            $leadProgression['PageURL3']  = '';
            $leadProgression['PageDate3']  = '';
            $leadProgression['PageStage3']  = 'Started<br />Deciding';
        }
        
        if ($count_converted>0){
            $leadProgression['PageTitle4']  = $lead_converted->PageName;
            $leadProgression['PageURL4']  = $lead_converted->PageURL;
            $leadProgression['PageDate4']  = date("M d, Y",strtotime($lead_converted->Date));
            $leadProgression['PageStage4']  = 'Converted';
        }else{
            $leadProgression['PageTitle4']  = '';
            $leadProgression['PageURL4']  = '';
            $leadProgression['PageDate4']  = '';
            $leadProgression['PageStage4']  = 'Converted';
        }

        $data['userDetails'] = $userDetails;
        $data['behaviorData'] = $behaviorData;
        $data['dailyActivity'] = $dailyActivity;
        $data['sessionProfile'] = $sessionProfile;
        $data['leadProgression'] = $leadProgression;

        return response()->json($data);
    }

    public function getLeadsData()
    {
        $client_id=Session('client_id');
        $leads_10 = Leads::where('client_id', $client_id)->distinct('e_id')->groupBy('e_id')->orderBy('Date', 'DESC')->take(20)->get();
        for ($i=0; $i < 20; $i++) { 
            $recentLeads['LeadList'][] = array(
                                            'UserID' => $leads_10[$i]->e_id,
                                            'LastSeen' => $leads_10[$i]->last_seen,
                                            'Channel' => $leads_10[$i]->browser,
                                            'LatestContent' => $leads_10[$i]->PageName,
                                            'Cookie' => $leads_10[$i]->has_cookies,
                                            'Stage' => $leads_10[$i]->Stage,
                                            'Date'=>$leads_10[$i]->Date
                                        );
        }
        
        return view('leads', [
            'client_id'=>$client_id,
            'recentLeads' => $recentLeads
        ]);
    }

    public function leadsFunnel()
    {
        $client_id = Input::get('clientID');
        $days = Input::get('days');
        $fromDate = date("Y-m-d");
        $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));

        $total_sessions = Leads::where('client_id', $client_id)
        ->whereBetween('Date', array($toDate, $fromDate))
        ->count();
        $AnonymousProspects = Leads::select(DB::raw('count(DISTINCT e_id) as cookies'))
        ->where('client_id', $client_id)
        ->whereBetween('Date', array($toDate, $fromDate))
        ->first(); 
        $leads_count = Leads::where('client_id', $client_id)
        ->whereBetween('Date', array($toDate, $fromDate))
        ->distinct('e_id')->groupBy('e_id')->count();

        $total_with_cookies = $total_sessions + $AnonymousProspects->cookies + $leads_count;

        $data['barChart1'][] = array(
                                    'SessionTitle' => 'Total Session',
                                    'TotalSession' => $total_sessions,
                                    'SessionPer' => (($total_sessions * 100) / $total_with_cookies),
                                    'SessionColor' => '#fc5d56'
                                );
        $data['barChart1'][] = array(
                                    'SessionTitle' => 'Anonymous Prospects',
                                    'TotalSession' => $AnonymousProspects->cookies,
                                    'SessionPer' => (($AnonymousProspects->cookies * 100) / $total_with_cookies),
                                    'SessionColor' => '#327aba'
                                );
        $data['barChart1'][] = array(
                                    'SessionTitle' => 'Leads',
                                    'TotalSession' => $leads_count,
                                    'SessionPer' => (($leads_count * 100) / $total_with_cookies),
                                    'SessionColor' => '#31ca6a'
                                );


        $session_withoutcookies = Leads::select(DB::raw('count(e_id) as count'))->where([['client_id', $client_id],['has_cookies', 'false']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->first();

        $prospects_withoutcookies = Leads::select(DB::raw('count(DISTINCT e_id) as count'))
        ->where([['client_id', $client_id], ['has_cookies','false']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->first();

        $leads_withoutcookies = Leads::select(DB::raw('count(e_id) as count'))->where([['client_id', $client_id], ['conversion', '1'], ['has_cookies', 'false']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->first(); 


        $total_without_cookies = $session_withoutcookies->count + $prospects_withoutcookies->count + $leads_withoutcookies->count;

        $data['barChart2'][] = array(
                                    'SessionTitle' => 'Session without cookies',
                                    'TotalSession' => $session_withoutcookies->count,
                                    'SessionPer' => (($session_withoutcookies->count * 100) / $total_without_cookies),
                                    'SessionColor' => '#fc5d56'
                                );
        $data['barChart2'][] = array(
                                    'SessionTitle' => 'Prospects without cookies',
                                    'TotalSession' => $prospects_withoutcookies->count,
                                    'SessionPer' => (($prospects_withoutcookies->count * 100) / $total_without_cookies),
                                    'SessionColor' => '#327aba'
                                );
        $data['barChart2'][] = array(
                                    'SessionTitle' => 'Leads without cookies',
                                    'TotalSession' => $leads_withoutcookies->count,
                                    'SessionPer' => (($leads_withoutcookies->count * 100) / $total_without_cookies),
                                    'SessionColor' => '#31ca6a'
                                );

        $allow_cookies = Leads::where([['client_id', $client_id],['allow_cookies','true']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->distinct('e_id')->groupBy('e_id')->count();

        $data['cookieUsage'][] = array(
                                    'value' => $allow_cookies,
                                    'color' => '#3c8dbc',
                                    'highlight' => '#3c8dbc',
                                    'label' => 'Leads that allow cookies'
                                );

        $data['cookieUsage'][] = array(
                                    'value' => $leads_count-$allow_cookies,
                                    'color' => 'red',
                                    'highlight' => 'red',
                                    'label' => 'Leads that block cookies'
                                );
        
       
        return response()->json($data);
    }

    public function leadsBreakdown()
    {
        $client_id = Input::get('clientID');
        $days = Input::get('days');
        $fromDate = date("Y-m-d");
        $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));
        
        $Date = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));
        $x = 0; 


        $barchart2 = Leads::select(DB::raw('count(e_id) as count, count(DISTINCT e_id) as visitors'),'Date')->where([['client_id', $client_id]])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->groupBy('Date')
        ->get();

        $leads = array();$visitors  = array();
        foreach ($barchart2 as $key => $value) {
            $leads[$value->Date] = $value->count;
            $visitors[$value->Date] = $value->visitors;
        }
        
        while (strtotime($Date) <= strtotime($fromDate)) {
          
                $addStr = '';
                if($x == 0 || $x == $days)
                    $addStr = substr(date("M",strtotime($Date)), 0, 1);

                $data['totalVisitsUniqueLeads'][] = array(
                                                        'y' => $addStr.date("d",strtotime($Date)),
                                                        'a' => array_key_exists($Date, $leads)?$leads[$Date]:0, //$barchart->count,
                                                        'b' => array_key_exists($Date, $visitors)?$visitors[$Date]:0 //$barchart->visitors
                                                    );
                $Date = date ("Y-m-d", strtotime("+1 day", strtotime($Date)));

                $x++;
        }

        $OS_windows_count = Leads::where([['client_id', $client_id],['OS_name','Windows']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->distinct('e_id')->groupBy('e_id')->count();

        $OS_mac_count = Leads::where([['client_id', $client_id],['OS_name','Mac']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->distinct('e_id')->groupBy('e_id')->count();

        $OS_ios_count = Leads::where([['client_id', $client_id],['OS_name','iOS']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->distinct('e_id')->groupBy('e_id')->count();

        $OS_android_count = Leads::where([['client_id', $client_id],['OS_name','Android']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->distinct('e_id')->groupBy('e_id')->count();

        $data['osUsage'][] = array(
                                    'value' => $OS_windows_count,
                                    'color' => '#3c8dbc',
                                    'highlight' => '#3c8dbc',
                                    'label' => 'Windows OS'
                                );

        $data['osUsage'][] = array(
                                    'value' => $OS_mac_count,
                                    'color' => '#dd4b39',
                                    'highlight' => '#dd4b39',
                                    'label' => 'Mac OSX'
                                );

        $data['osUsage'][] = array(
                                    'value' => $OS_ios_count,
                                    'color' => '#00a65a',
                                    'highlight' => '#00a65a',
                                    'label' => 'iOS'
                                );

        $data['osUsage'][] = array(
                                    'value' => $OS_android_count,
                                    'color' => '#f39c12',
                                    'highlight' => '#f39c12',
                                    'label' => 'Anroid OS'
                                );
        
       
        return response()->json($data);
    }

    public function channelsDrivingConversion()
    {
        $client_id = Input::get('clientID');
        $days = Input::get('days');
        $fromDate = date("Y-m-d");
        $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));

        $channels_facebook = Leads::where([['client_id', $client_id],['domainname','facebook']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->count();
        $channels_twitter = Leads::where([['client_id', $client_id],['domainname','twitter']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->count();
        $channels_google = Leads::where([['client_id', $client_id],['domainname','google']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->count();
        $channels_all = Leads::where('client_id', $client_id)
        ->whereBetween('Date', array($toDate, $fromDate))
        ->count();
        // $channels_facebook = Leads::where([['client_id', $client_id],['domainname','facebook']])->groupBy('e_id')->count();
        // $channels_twitter = Leads::where([['client_id', $client_id],['domainname','twitter']])->groupBy('e_id')->count();
        // $channels_google = Leads::where([['client_id', $client_id],['domainname','google']])->groupBy('e_id')->count();
        // $channels_all = Leads::where('client_id', $client_id)->groupBy('e_id')->count();
        


        $channels_facebook_conversion = Leads::where([['client_id', $client_id],['domainname','facebook']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->distinct('e_id')->count();
        $channels_twitter_conversion = Leads::where([['client_id', $client_id],['domainname','twitter']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->distinct('e_id')->count();
        $channels_google_conversion = Leads::where([['client_id', $client_id],['domainname','google']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->distinct('e_id')->count();
        $channels_all_conversion = Leads::where('client_id', $client_id)->distinct('e_id')
        ->whereBetween('Date', array($toDate, $fromDate))
        ->count();
        // $channels_facebook_conversion = Leads::where([['client_id', $client_id],['domainname','facebook']])->distinct('e_id')->groupBy('e_id')->count();
        // $channels_twitter_conversion = Leads::where([['client_id', $client_id],['domainname','twitter']])->distinct('e_id')->groupBy('e_id')->count();
        // $channels_google_conversion = Leads::where([['client_id', $client_id],['domainname','google']])->distinct('e_id')->groupBy('e_id')->count();
        // $channels_all_conversion = Leads::where('client_id', $client_id)->distinct('e_id')->groupBy('e_id')->count();

        $data['channelData1'] = array(
                                    'ChannelName' => 'Facebook',
                                    'ChannelText' => 'Change in Users',
                                    'IsChannelUp' => true,
                                    'ChannelPer' => strval(round($channels_facebook/$channels_all*100)).'%',
                                    'ConversionRate' => strval(round($channels_facebook_conversion/$channels_all_conversion*100)).'%',
                                    'ProspectsGenerated' => $channels_facebook,
                                    'LeadConversions' => $channels_facebook_conversion
                                );

        $data['channelData2'] = array(
                                    'ChannelName' => 'Twitter',
                                    'ChannelText' => 'Change in Users',
                                    'IsChannelUp' => true,
                                    'ChannelPer' => strval(round($channels_twitter/$channels_all*100)).'%',
                                    'ConversionRate' => strval(round($channels_twitter_conversion/$channels_all_conversion*100)).'%',
                                    'ProspectsGenerated' => $channels_twitter,
                                    'LeadConversions' => $channels_twitter_conversion
                                );

        $data['channelData3'] = array(
                                    'ChannelName' => 'Google',
                                    'ChannelText' => 'Change in Users',
                                    'IsChannelUp' => true,
                                    'ChannelPer' => strval(round($channels_google/$channels_all*100)).'%',
                                    'ConversionRate' => strval(round($channels_google_conversion/$channels_all_conversion*100)).'%',
                                    'ProspectsGenerated' => $channels_google,
                                    'LeadConversions' => $channels_google_conversion
                                );

        return response()->json($data);
    }

    public function contentDrivingConversion()
    {
        $client_id = Input::get('clientID');
        $days = Input::get('days');
        $fromDate = date("Y-m-d");
        $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));

        $conversion = ContentConversion::select(DB::raw('count(DISTINCT PageName) as Prospects,sum(Value) as score, PageName,PageURL, Date'))->where([['client_id', $client_id],['conversion','1']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->groupBy('PageName')->orderBy('score', 'DESC')->take(3)->get();

        $data = array();

        if(count($conversion) > 0){

            $data['contentData1'] = array(
                                        'ContentTitle' => substr($conversion[0]->PageName, 0, 20).'...',
                                        'ContentUrl' => $conversion[0]->PageURL,
                                        'ContentPer' => $conversion[0]->score,
                                        'ProspectsGenerated' => $conversion[0]->Prospects,
                                        'Posted' => date("M d, Y",strtotime($conversion[0]->Date))
                                    );
        }
        if(count($conversion) > 1){
            $data['contentData2'] = array(
                                        'ContentTitle' => substr($conversion[1]->PageName, 0, 20).'...',
                                        'ContentUrl' => $conversion[1]->PageURL,
                                        'ContentPer' => $conversion[1]->score,
                                        'ProspectsGenerated' => $conversion[1]->Prospects,
                                        'Posted' => date("M d, Y",strtotime($conversion[1]->Date))
                                    );
        }
        if(count($conversion) > 2){
            $data['contentData3'] = array(
                                        'ContentTitle' => substr($conversion[2]->PageName, 0, 20).'...',
                                        'ContentUrl' => $conversion[2]->PageURL,
                                        'ContentPer' => $conversion[2]->score,
                                        'ProspectsGenerated' => $conversion[2]->Prospects,
                                        'Posted' => date("M d, Y",strtotime($conversion[2]->Date))
                                    );
        }
        return response()->json($data);
    }
}
