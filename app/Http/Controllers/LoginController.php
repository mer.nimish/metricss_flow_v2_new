<?php

namespace App\Http\Controllers;
 
use Auth;
use Illuminate\Http\Request;
use DB;
use App\UserAdmin as UserAdmin;
use App\User as User;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;

class LoginController extends Controller
{
    use AuthenticatesUsers {
        AuthenticatesUsers::login as traitLogin;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';//Here is home page.

    /**
     * Create a new login controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        if (Session('email')){
            return redirect('dashboard');
        }
        else{
            return view('login');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function login(Request $request)
    {
        if ($request->has('remember')) {
            $request->session()->put('Auth:auth-remember', $request->remember);

            $request->merge(['remember' => '']);
        }
       
        $user=UserAdmin::where([['email', $request->email],["password", $request->password]])->first();

        //$user=UserAdmin::where('email', $request->email)->first();
         
        if (count($user)>0)
            { 
                $request->session()->put('userdata', $user);
                $request->session()->put('email', $request->email);
                $request->session()->put('client_id', $user->client_id);
                return redirect('dashboard');
            }
            else{
                return redirect('/');
            }
            ;


        // if ($request->email=="bushfurniture@admin.com" && $request->password=="bushadmin"){
                
        //         return redirect('dashboard');
        //     }
        // else{ return redirect('/');
        // }
         
    }

    public function logout(Request $request)
    {
        Session::flush();
        return redirect('/');

    }

}
