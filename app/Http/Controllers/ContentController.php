<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use DB;
use App\Leads as Leads;
use App\TopContents as TopContents;
use App\ContentTopEntry as ContentTopEntry;
use App\ContentActionItem as ContentActionItem;

use Carbon\Carbon;



class ContentController extends Controller
{
    public function index(Request $request)
    {
        $client_id=Session('client_id');
        $PageName=$request->PageName;
        $PostDate=$request->Date;

        $date = Carbon::now();
        $date->subDays(30);
        $current_contents=TopContents::where('PageName', $PageName)->orderBy('Date')->get();
        //$contents_score=TopContents::where('PageName', $PageName)->sum('score');

        $content_Awareness = TopContents::where([['client_id', $client_id],['Stage','Awareness'],['PageName', $PageName]])->sum('Value');
        $content_Engagement = TopContents::where([['client_id', $client_id],['Stage','Engaged'],['PageName', $PageName]])->sum('Value');
        $content_Consideration = TopContents::where([['client_id', $client_id],['Stage','Consideration'],['PageName', $PageName]])->sum('Value');
        $content_Conversion = TopContents::where([['client_id', $client_id],['Stage','Converted'],['PageName', $PageName]])->sum('Value');


        
        // $dount_google=ContentTopEntry::where([ ['domainname','google'],['PageName', $PageName]])->sum('freq');
        // $dount_bing=ContentTopEntry::where([ ['domainname','bing'],['PageName', $PageName]])->sum('freq');
        // $dount_yahoo=ContentTopEntry::where([ ['domainname','yahoo'],['PageName', $PageName]])->sum('freq');
        // $dount_ask=ContentTopEntry::where([ ['domainname','ask'],['PageName', $PageName]])->sum('freq');
        // $dount_dogpile=ContentTopEntry::where([ ['domainname','dogpile'],['PageName', $PageName]])->sum('freq');
        $dount_google=ContentTopEntry::where('domainname','google')->sum('freq');
        $dount_bing=ContentTopEntry::where('domainname','bing')->sum('freq');
        $dount_yahoo=ContentTopEntry::where('domainname','yahoo')->sum('freq');
        $dount_ask=ContentTopEntry::where('domainname','ask')->sum('freq');
        $dount_dogpile=ContentTopEntry::where('domainname','dogpile')->sum('freq');

        $topentrys=ContentTopEntry::where('PageName', $PageName)->orderBy('Date')->get();
        $dategroup=ContentTopEntry::where('PageName', $PageName)->orderBy('Date')->groupBy('Date')->get();

        $actionitems=ContentActionItem::where('PageName', $PageName)->orderBy('TotalVisitors')->first();

        return view('ContentDetail', [
            'client_id'=>$client_id,
            'PageName' => $PageName,
            'PostDate' => $PostDate,
            'current_contents'=>$current_contents,
            /*'contents_score'=>$contents_score,*/
            
            'content_Awareness'=>$content_Awareness,
            'content_Engagement'=>$content_Engagement,
            'content_Consideration'=>$content_Consideration,
            'content_Conversion'=>$content_Conversion,

            'dount_google'=>$dount_google,
            'dount_bing'=>$dount_bing,
            'dount_yahoo'=>$dount_yahoo,
            'dount_ask'=>$dount_ask,
            'dount_dogpile'=>$dount_dogpile,

            'topentrys'=>$topentrys,
            'dategroup'=>$dategroup,
            'actionitems'=>$actionitems
        ]);
    }

    

    public function ShowContents()
    {
        $client_id=Session('client_id');
        $contents = TopContents::where('client_id', $client_id)->distinct('PageName')->groupBy('PageName')->orderBy('Date', 'DESC')->take(20)->get();

        // $top_contents = Content::selectRaw('PageName,Date,score,PageURL, sum(Value) as sum,round(avg(score),1) as avgscore')->where([['client_id', Auth::user()->client_id],['Date','>=', $filterdate]])->groupBy('PageName')->orderBy('sum', 'DESC')->take(3)->get();
        $top_contents_Awareness = TopContents::selectRaw('PageName,Date,score,PageURL, sum(Value) as sum,round(avg(score),1) as avgscore')->where([['client_id', $client_id],['Stage','Awareness']])->groupBy('PageName')->orderBy('sum', 'DESC')->take(6)->get();
        $top_contents_Engagement = TopContents::selectRaw('PageName,Date,score,PageURL, sum(Value) as sum,round(avg(score),1) as avgscore')->where([['client_id', $client_id],['Stage','Engaged']])->groupBy('PageName')->orderBy('sum', 'DESC')->take(6)->get();
        $top_contents_Consideration = TopContents::selectRaw('PageName,Date,score,PageURL, sum(Value) as sum,round(avg(score),1) as avgscore')->where([['client_id', $client_id],['Stage','Consideration']])->groupBy('PageName')->orderBy('sum', 'DESC')->take(6)->get();
        $top_contents_Conversion = TopContents::selectRaw('PageName,Date,score,PageURL, sum(Value) as sum,round(avg(score),1) as avgscore')->where([['client_id', $client_id],['Stage','Converted']])->groupBy('PageName')->orderBy('sum', 'DESC')->take(6)->get();
        
        
        return view('Content', [
            'client_id'=>$client_id,
            'contents' => $contents,
            'top_contents_Awareness'=>$top_contents_Awareness,
            'top_contents_Engagement'=>$top_contents_Engagement,
            'top_contents_Consideration'=>$top_contents_Consideration,
            'top_contents_Conversion'=>$top_contents_Conversion
        ]);
    }

    public function performance(Request $request)
    {
        $clientID = Session('client_id');
        $days = Input::get('days');
        $fromDate = date("Y-m-d");
        $PageName=$request->PageName;
        $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));

        $current_contents = TopContents::where('PageName', $PageName)->orderBy('Date')->get();
        $contents_score = TopContents::where('PageName', $PageName)->sum('score');

        $contents = array();
        foreach ($current_contents as $key => $value) {
            $contents[$value->Date] = $value->Value;
            $page = substr($value->PageName,-20);
        }

        $x = 0;  $ProspectsGenerated = 0;
        while (strtotime($toDate) <= strtotime($fromDate)) {

                $content_value = array_key_exists($toDate, $contents)?$contents[$toDate]:0;

                $jqChartdata1[] = array(date('M d',strtotime($toDate)),$content_value);
                /*$jqChartdata2[] = array(date('Md',strtotime($toDate)),10+$x);*/


                $ProspectsGenerated = $ProspectsGenerated + $content_value;

                $toDate = date ("Y-m-d", strtotime("+1 day", strtotime($toDate)));

                $x++;
        }

        $data['jqChart'][] = array(
                                    'type' => 'area',
                                    'title' => $page,
                                    'fillStyle' => '#2d69a0',
                                    'data' => $jqChartdata1
                                );
      /*  $data['jqChart'][] = array(
                                    'type' => 'area',
                                    'title' => 'Prospects',
                                    'fillStyle' => '#327aba',
                                    'data' => $jqChartdata2
                                );*/

        $data['Trending'] = number_format($contents_score,2);
        $data['ProspectsGenerated'] = number_format($ProspectsGenerated,2);

        return response()->json($data);
    }

    public function engagement(Request $request)
    {
        $client_id = Session('client_id');
        $days = Input::get('days');
        $fromDate = date("Y-m-d");
        $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));
        $PageName=$request->PageName;


        $content_Awareness = TopContents::where([['client_id', $client_id],['Stage','Awareness'],['PageName', $PageName]])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->sum('Value');
        $content_Engagement = TopContents::where([['client_id', $client_id],['Stage','Engaged'],['PageName', $PageName]])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->sum('Value');
        $content_Consideration = TopContents::where([['client_id', $client_id],['Stage','Consideration'],['PageName', $PageName]])->whereBetween('Date', array($toDate, $fromDate))
            ->sum('Value');
        $content_Conversion = TopContents::where([['client_id', $client_id],['Stage','Converted'],['PageName', $PageName]])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->sum('Value');

        $data['AwarenessValue'] = $content_Awareness;
        $data['AwarenessFlag'] = 'up';
        $data['EngagementValue'] = $content_Engagement;
        $data['EngagementFlag'] = 'down';
        $data['ConsiderationValue'] = $content_Consideration;
        $data['ConsiderationFlag'] = 'down';
        $data['ConvertedValue'] = $content_Conversion;
        $data['ConvertedFlag'] = 'up';

        return response()->json($data);
    }

    public function topEntryPoints(Request $request)
    {
        $client_id = Session('client_id');
        $months = Input::get('months');
        $fromDate = date("Y-m-d");
        $PageName=$request->PageName;

        $topentrys = ContentTopEntry::select('*', DB::raw('count(domainname) as topentry'))->where('PageName', $PageName)->groupBy('domainname')->groupBy(DB::raw('YEAR(Date)'))->groupBy(DB::raw('MONTH(Date)'))->orderBy(DB::raw('count(domainname)'))->get();

        /*$topentrys2 = DB::select('SELECT *, count(domainname) FROM topentry WHERE PageName = "300 Series Collection by Bush Furniture" GROUP BY domainname, YEAR(Date), MONTH(Date) ORDER by count(domainname) desc');*/
       
        $domainname = array(); $topentrydata = array();
        foreach ($topentrys as $key => $value) {
            $domainname[$value->domainname] = $value->domainname;
            $topentrydata[$value->domainname][date('Y-m', strtotime($value->Date))] = $value->topentry;
            $months_data[date('Y-m', strtotime($value->Date))] = date('Y-m', strtotime($value->Date));
        }

        $title_arr = $domainname; //array('Google','Bing','Yahoo','Ask','dogpile');
        $color_arr = array('#3c8dbc','#dd4b39','#00a65a','#f39c12','#272a52');
       
        $z = 0; 
        foreach ($title_arr as $key => $value) {

            $month = strtotime($fromDate);
            $i = 1;
            $pieChartTotalPoint = 0; 
            $barChartPoints = array();
            while($i <= $months)
            {
                $month_name[] = date('M', $month);
                $month = strtotime('-1 month', $month);
                $datapt = in_array(date('Y-m', $month), $months_data)?$topentrydata[$value][date('Y-m', $month)]:0;
                $barChartPoints[] = $datapt;
                $pieChartTotalPoint += $datapt;
                $i++;
            } 

            $data['pieChartPoints'][] = array(
                                    'value' => $pieChartTotalPoint,
                                    'color' => $color_arr[$z],
                                    'highlight' => $color_arr[$z],
                                    'label' => $value,
                                );
            
            $data['barChartPoints'][] = array(
                                        'type' => 'column',
                                        'title' => $value,
                                        'fillStyle' => $color_arr[$z],
                                        'data' => $barChartPoints
                                    );
            

            if($z == 4)
                break;

            $z++;
        }

        $data['barChartAxes'][] = array(
                                    'type' => 'category',
                                    'location' => 'bottom',
                                    'categories' => $month_name
                                    );

        return response()->json($data);
    }

    public function topPerformingContent(Request $request)
    {
        $client_id = Session('client_id');
        $days = Input::get('days');
        $fromDate = date("Y-m-d");
        $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));

        $top_contents_Awareness = TopContents::selectRaw('PageName,Date,score,PageURL, sum(Value) as sum,round(avg(score),1) as avgscore')->where([['client_id', $client_id],['Stage','Awareness']])
         ->whereBetween('Date', array($toDate, $fromDate))
         ->groupBy('PageName')->orderBy('sum', 'DESC')->take(6)->get();

        foreach ($top_contents_Awareness as $key => $content){

            $data['AwarenessData'][] = array(
                                        'PageName' =>  $content->PageName,
                                        'Date' => $content->Date,
                                        'PageURL' =>  $content->PageURL,
                                        'avgscore' =>  $content->avgscore,
                                        'sum' =>  $content->sum
                                    );
        }

        $top_contents_Engagement = TopContents::selectRaw('PageName,Date,score,PageURL, sum(Value) as sum,round(avg(score),1) as avgscore')->where([['client_id', $client_id],['Stage','Engaged']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->groupBy('PageName')->orderBy('sum', 'DESC')->take(6)->get();

        foreach ($top_contents_Engagement as $key => $content){

            $data['EngagementData'][] = array(
                                        'PageName' =>  $content->PageName,
                                        'Date' => $content->Date,
                                        'PageURL' =>  $content->PageURL,
                                        'avgscore' =>  $content->avgscore,
                                        'sum' =>  $content->sum
                                    );
        }

        $top_contents_Consideration = TopContents::selectRaw('PageName,Date,score,PageURL, sum(Value) as sum,round(avg(score),1) as avgscore')->where([['client_id', $client_id],['Stage','Consideration']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->groupBy('PageName')->orderBy('sum', 'DESC')->take(6)->get();

        foreach ($top_contents_Consideration as $key => $content){

            $data['ConsiderationData'][] = array(
                                        'PageName' =>  $content->PageName,
                                        'Date' => $content->Date,
                                        'PageURL' =>  $content->PageURL,
                                        'avgscore' =>  $content->avgscore,
                                        'sum' =>  $content->sum
                                    );
        }

        $top_contents_Conversion = TopContents::selectRaw('PageName,Date,score,PageURL, sum(Value) as sum,round(avg(score),1) as avgscore')->where([['client_id', $client_id],['Stage','Converted']])
        ->whereBetween('Date', array($toDate, $fromDate))
        ->groupBy('PageName')->orderBy('sum', 'DESC')->take(6)->get();

        foreach ($top_contents_Conversion as $key => $content){

            $data['ConversionData'][] = array(
                                        'PageName' =>  $content->PageName,
                                        'Date' => $content->Date,
                                        'PageURL' =>  $content->PageURL,
                                        'avgscore' =>  $content->avgscore,
                                        'sum' =>  $content->sum
                                    );
        }

        return response()->json($data);
    }
    
}
