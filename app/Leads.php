<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leads extends Model
{
    protected $table = 'LeadDNA';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
