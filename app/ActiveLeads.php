<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveLeads extends Model
{
    protected $table = 'livelead';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
