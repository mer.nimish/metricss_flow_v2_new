<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAdmin extends Model
{
    protected $table = 'users';

    public $timestamps = false;

    protected $connection = 'mysql';


}
