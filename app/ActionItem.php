<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionItem extends Model
{
    protected $table = 'ActionItem';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
