<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NodeTransition extends Model
{
    protected $table = 'NodeTEST';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
