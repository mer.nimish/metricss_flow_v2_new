@extends('template.template')

@section('content-header')
<link rel="stylesheet" href="{{URL::to('/css/ContentDetail.css')}}">
<section class="content-header">
	<div id="breadcrumbs">
		<h4 class="bold"><i class="fa fa-chevron-left" aria-hidden="true"></i>Campains
			</h4>
	</div> 
	<div id="client_logo">
		<img src="img/{{$client_id}}.png" alt="client_logo" >
	</div>
</section>
@endsection
@section('content')
<div class="main-content">
<div class="row">
<section id="TOPchannels">
	<div class="row">
		<div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
			<h2 class="bold">{{$PageName}}</h2>
			<p>Posted: {{$PostDate}}</p>
		</div>
	</div>
</section>
<section id="Summary">
	<div class="row">
		<div class="col-lg-11 col-md-10 col-xs-12 col-sm-12">
			<div class="nav-tabs-custom" >
			    <ul class="nav nav-tabs ">
			          <li class="active"><a href="#tab_1" data-toggle="tab"><h3>Summary</h3></a></li>
			          <li><a href="#tab_2" data-toggle="tab"><h3>Engagement</h3></a></li>
			    </ul>
			    <div class="tab-content light-blue">
					<div class="tab-pane active" id="tab_1">
						<section id="TopKeywordsUsed">
							    <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
									<h3 class="bold">Performance</h3>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
										<select class="form-control tableselect" id="seldays"  onchange="performance(this.value)">
							                <option value="7">Last 7 days</option>
							              	<option value="14">Last 14 days</option>
							              	<option value="21">Last 21 days</option>
							              	<option value="30">Last 30 days</option>

								        </select>
								     </div> 
								</div>    
					
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								    
								<div class="alert-modal">
									<div class="modal">
										<div class="row">
											<div class="modal-dialog">
											    <div class="modal-content">
											        <div class="modal-header light-blue">
											        	<div class="col-md-12 box1">
								                            <div id="jqChart" style="width:95%; height: 250px; margin-left: 20px;">
												    		  	<div class="col-xs-4 col-sm-4 col-lg-2 col-sm-4 text-right" style="color: #59f441">
											            			<p class="c_score"><strong id="P_Score"><?php /* {{number_format($contents_score,1)}} */ ?></strong></p>
											            		</div>		 
											            		<div class="col-xs-8 col-sm-8 col-lg-3 col-sm-8">
											            			 <p class="c_trending"><strong>TRENDING UP</strong></p>
											            			 <p class="c_prospects">Prospects Generated: <span id="P_Prospects"></span></p>
											            		</div>	
												    		</div>
												        </div>
												        <!-- <div class="col-md-12 col-lg-12 col-sm-12 col-md-offset-1">	
												        	<div class="col-md-2">
												        		<i class="fa fa-stop fa-1x  labeltext" style="color:#2d69a0; margin-top: 5px; "></i> Sessions
												       		</div>
												       		<div class="col-md-2">
												        		<i class="fa fa-stop fa-1x  labeltext" style="color:#327aba; margin-top: 5px; "></i> Prospects
												       		</div>
												       		<div class="col-md-2">
												        		<i class="fa fa-stop fa-1x  labeltext" style="color:#272a52; margin-top: 5px; "></i> Leads
												       		</div>
												        </div> -->
											        </div>
											    </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>

						<section id="TopKeywordsUsed">

							<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
								<h3 class="bold">Engagement</h3>
							</div>
							<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
								<select class="form-control tableselect" id="seldays"  onchange="engagement(this.value)">
					                <option value="7">Last 7 days</option>
					              	<option value="14">Last 14 days</option>
					              	<option value="21">Last 21 days</option>
					              	<option value="30">Last 30 days</option>
						        </select>
						     </div>   

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								    
								<div class="alert-modal">
									<div class="modal">
										<div class="row">
											<div class="modal-dialog">
											    <div class="modal-content" style="height:350px">
											        <div class="modal-header light-blue" > 
											        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="top:34px;">
												        	<div id="sq_chart1">
												        		<center id="E_AwarenessDetails">
												        			<i class="fa fa-arrow-up fa-4x white-trend" aria-hidden="true">{{$content_Awareness}}</i>
												        		</center>
												        	</div>
												        	<div id="sq_chart2">
												        		<center id="E_EngagementDetails">
												        			<i class="fa fa-arrow-down fa-4x white-trend" aria-hidden="true">{{$content_Engagement}}</i>
												        		</center>
												        	</div>
												        	<div id="sq_chart3">
												        		<center id="E_ConsiderationDetails">
												        			<i class="fa fa-arrow-down fa-4x white-trend" aria-hidden="true">{{$content_Consideration}}</i>
												        		</center>
												        	</div>
												        	<div id="sq_chart4">
												        		<center id="E_ConvertedDetails">
												        			<i class="fa fa-arrow-up fa-4x white-trend" aria-hidden="true">{{$content_Conversion}}</i>
												        		</center>
												        	</div>
											        	</div>
											         </div> 
											        
											    </div>
											    <div class="modal-footer">
											        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
											        		<div class="col-xs-3">
											        			<center><p>Awareness</p></center>
											        		</div>
											        		<div class="col-xs-3">
											        			<center><p>Engagement</p></center>
											        		</div>
											        		<div class="col-xs-3">
											        			<center><p>Consideration</p></center>
											        		</div>
											        		<div class="col-xs-3">
											        			<center><p>Converted</p></center>
											        		</div>
											        	</div>
											        </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>


						<section id="TopKeywordsUsed">

							<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
								<h3 class="bold">Top entry Points</h3>
							</div>
							<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
								<select class="form-control tableselect" onchange="topEntryPoints(this.value)">
						            <option value="6">Last 6 Months</option>
					              	<option value="3">Last 3 Months</option>
					              	<option value="1">Last 1 Months</option>
						        </select>
						     </div>   

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  
								    
								<div class="alert-modal">
									<div class="modal">
										<div class="row">
											<div class="modal-dialog">
											    <div class="modal-content">
											        
											        <div class="modal-header light-blue
											        ">
											        	<div class="navgroup nav-tabs-custom col-lg-2 col-md-4 col-sm-12 col-xs-12" >
														    <ul class="nav nav-tabs entry">
														          <li class="active"><a href="" data-toggle="tab"><h4>Social Media</h4></a></li>
														          <li><a href="" data-toggle="tab"><h4>Search Engines</h4></a></li>
														          <li><a href="" data-toggle="tab"><h4>Email Campains</h4></a></li>
														          <li><a href="" data-toggle="tab"><h4>PPC</h4></a></li>
														          <li><a href="" data-toggle="tab"><h4>Refferal Links</h4></a></li>
														    </ul>
														</div>
														<div class="col-lg-10 col-md-8 col-sm-12 col-xs-12" style="height:100%">
															<div class="row"><br><br></div>
															<div class="row">
																<div class="col-md-4 ">
																	<!-- <div id="donut-chart">
																	</div> -->
																	<canvas id="donut-chart" height="150"></canvas>
																</div>
																<div class="col-md-8 ">
																	<div id="jqChart_bar" class="row">
																	</div>
																	<!-- <div class="row">
																		<div class="float-right">
																		<i class="fa fa-stop fa-1x  labeltext" style="color:#31ca6a; margin-top: 5px; "></i> <span style="margin-right: 20px; ">Google </span> 
																		<i class="fa fa-stop fa-1x  labeltext" style="color:#327aba; margin-top: 5px; "></i> <span style="margin-right: 20px; ">Bing   </span> 
																		<i class="fa fa-stop fa-1x  labeltext" style="color:#e70047; margin-top: 5px; "></i> <span style="margin-right: 20px; ">Yahoo   </span> 
																		<i class="fa fa-stop fa-1x  labeltext" style="color:#272a52; margin-top: 5px; "></i> <span style="margin-right: 20px; ">Ask   </span> 
																		<i class="fa fa-stop fa-1x  labeltext" style="color:#f5f7ff; margin-top: 5px; "></i> <span style="margin-right: 20px; ">Dogpile  </span>   
																		</div>
																	</div> -->

																</div>

															</div>

														</div>
											        	
											        </div>
											    </div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</section>


					</div>
					<div class="tab-pane" id="tab_2">
						<section id="ActionItems">
							<div class="row">
								<div class="col-md-12 col-lg-12 col-xs-12">
									<h3 class="bold">Action Items</h3>
								</div>
							</div>
							
							<!-- /.modal -->
							<div class="row">
								<div class="col-md-12 col-lg-12 col-xs-12">
									<div class="alert-modal">
										<div class="modal">
											<div class="col-md-12 col-xs-12 col-sm-12">
												<div class="modal-dialog">
												    <div class="modal-content">
												        <div class="modal-header red">
												        	<div class="col-md-12 col-xs-12 col-sm-12" style="padding: 0;">
													           	<div class="col-md-8 col-sm-8 col-xs-10">          
													            	<h4 class="modal-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Waring #1 - Button with very low click rate</h4>
													            </div>
													            <div class="col-md-4 col-sm-4 col-xs-2">
														            <div class="float-right">
														              	<button type="button" class="close alert-button" data-dismiss="modal" aria-label="Close" style="padding: 5px">
														                  <span aria-hidden="true">&times;</span></button>
														           	</div>
														        </div>
													        </div>
												        </div>
												        <div class="modal-body">
												        	<div class="row left">
													            <div class="col-md-6 col-xs-12 col-sm-12 alert-content left">
													            	<div class="alert-label">
													            		<h5>Only {{$actionitems->TotalVisitors}} clicks <strong>During the selected date range</strong></h5>
													            	</div>
													            </div>
													            <div class="col-md-3 col-xs-12 col-sm-12 alert-content left">
													            	<div class="alert-label">
													            		<h5>Target click rate: <strong>20%</strong></h5>
													            	</div>
													            </div>
													            <div class="col-md-3 col-xs-12 col-sm-12 alert-content left">
													            	<div class="alert-label">
													            		<h5>Actual click rate: <strong>0.5%</strong></h5>
													            	</div>
													            </div>
													            <div class="col-md-12 col-xs-12 col-sm-12 left15">
													            	<div class="alert-label">
													            		<h5 style="display: inline-block;">Suggestion: <strong>This button might not helpful to your funnel. Try chaging the call option, or perhaps experiment with the button removed entirely.</strong></h5> 
													            	</div>
													            </div>
													        </div>
												           
												        </div>
												    </div>
												    <!-- /.modal-content -->
												</div>
											</div>
										</div>
									    <!-- /.modal-dialog -->
									</div>
								</div>
							</div>
							<!-- /.modal -->
							<div class="row">
								<div class="col-md-12  col-lg-12 col-xs-12">
									<div class="alert-modal">
										<div class="modal">
											<div class="col-md-12 col-xs-12 col-sm-12">
												<div class="modal-dialog">
												    <div class="modal-content">
												        <div class="modal-header red">
												        	<div class="col-md-12 col-xs-12 col-sm-12" style="padding: 0;">
													           	<div class="col-md-8 col-sm-8 col-xs-10">          
													            	<h4 class="modal-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Waring #2 - From with very low submit rate</h4>
													            </div>
													            <div class="col-md-4 col-sm-4 col-xs-2">
														            <div class="float-right">
														              	<button type="button" class="close alert-button" data-dismiss="modal" aria-label="Close" style="padding: 5px">
														                  <span aria-hidden="true">&times;</span></button>
														           	</div>
														        </div>
													        </div>
												        </div>
												        <div class="modal-body">
												        	<div class="row left">
													            <div class="col-md-6 col-xs-12 col-sm-12 alert-content left">
													            	<div class="alert-label">
													            		<h5>Only {{$actionitems->TotalSessionsAttracted}} Submissions <strong>During the selected date range</strong></h5>
													            	</div>
													            </div>
													            <div class="col-md-3 col-xs-12 col-sm-12 alert-content left">
													            	<div class="alert-label">
													            		<h5>Target Submit rate: <strong>10%</strong></h5>
													            	</div>
													            </div>
													            <div class="col-md-3 col-xs-12 col-sm-12 alert-content left">
													            	<div class="alert-label">
													            		<h5>Actual Submit rate: <strong>2.2%</strong></h5>
													            	</div>
													            </div>
													            <div class="col-md-12 col-xs-12 col-sm-12 left15">
													            	<div class="alert-label">
													            		<h5 style="display: inline-block;">Suggestion: <strong>Lots of users start but don't finish completing the form, Try removing unnecessary from fields to help make the from quicker and simpler to fill out. Try changing the call to action on the subit button.</strong></h5> 
													            	</div>
													            </div>
													        </div>
												           
												        </div>
												    </div>
												    <!-- /.modal-content -->
												</div>
											</div>
										</div>
									    <!-- /.modal-dialog -->
									</div>
								</div>
							</div>
							<!-- /.modal -->
							<div class="row">
								<div class="col-md-12  col-lg-12 col-xs-12">
									<div class="alert-modal">
										<div class="modal">
											<div class="col-md-12 col-xs-12 col-sm-12">
												<div class="modal-dialog">
												    <div class="modal-content">
												        <div class="modal-header red">
												        	<div class="col-md-12 col-xs-12 col-sm-12" style="padding: 0;">
													           	<div class="col-md-8 col-sm-8 col-xs-10">          
													            	<h4 class="modal-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Waring #3 - Button with very low click rate</h4>
													            </div>
													            <div class="col-md-4 col-sm-4 col-xs-2">
														            <div class="float-right">
														              	<button type="button" class="close alert-button" data-dismiss="modal" aria-label="Close" style="padding: 5px">
														                  <span aria-hidden="true">&times;</span></button>
														           	</div>
														        </div>
													        </div>
												        </div>
												        <div class="modal-body">
												        	<div class="row left">
													            <div class="col-md-6 col-xs-12 col-sm-12 alert-content left">
													            	<div class="alert-label">
													            		<h5>Only {{$actionitems->TotalVisitors}} clicks <strong>During the selected date range</strong></h5>
													            	</div>
													            </div>
													            <div class="col-md-3 col-xs-12 col-sm-12 alert-content left">
													            	<div class="alert-label">
													            		<h5>Target click rate: <strong>15%</strong></h5>
													            	</div>
													            </div>
													            <div class="col-md-3 col-xs-12 col-sm-12 alert-content left">
													            	<div class="alert-label">
													            		<h5>Actual click rate: <strong>1.5%</strong></h5>
													            	</div>
													            </div>
													            <div class="col-md-12 col-xs-12 col-sm-12 left15">
													            	<div class="alert-label">
													            		<h5 style="display: inline-block;">Suggestion: <strong>This button might not helpful to your funnel. Try chaging the call option, or perhaps experiment with the button removed entirely.</strong></h5> 
													            	</div>
													            </div>
													        </div>
												           
												        </div>
												    </div>
												    <!-- /.modal-content -->
												</div>
											</div>
										</div>
									    <!-- /.modal-dialog -->
									</div>
								</div>
							</div>
						</section>
						<section id="ActionItems">
							<div class="row">
								<!-- <div class="col-md-12 col-lg-12 col-xs-12">
									<h3 class="bold">Visitor Engagement</h3>
								</div>
								<div class="col-md-12 col-lg-12 col-xs-12">
									<div class="col-md-6">
					  					<span>A.I. Overlay</span>
					  					<input type="button" id="heat_btn" style="border: none;background: url('img/heatmap0.png');width: 31px;height: 18px;float: left;" onclick="onheatmap();">
					  				</div>
									<div class="col-md-6 header">
										<select class="float-right">
							                <option>Last 30 days</option>
							              	<option>Last 60 days</option>
							              	<option>Last 90 days</option>
								        </select>
									</div>   
								</div> -->

								<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
									<h3 class="bold">Visitor Engagement</h3>
									<span>A.I. Overlay</span>
					  				<input type="button" id="heat_btn" style="border: none;background: url('img/heatmap0.png');width: 31px;height: 18px;float: left;" onclick="onheatmap();">
								</div>
								<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
									<select class="form-control tableselect">
						              	<option>Last 30 days</option>
						              	<option>Last 60 days</option>
						              	<option>Last 90 days</option>

							        </select>
							     </div> 
					
							</div>
							
							<!-- /.modal -->
							<div class="row">
								<div class="col-md-12 col-lg-12 col-xs-12">
									<div class="alert-modal">
										<div class="modal">
											<div class="col-md-12 col-xs-12 col-sm-12">
												<div class="modal-dialog">
												    <div class="modal-content">
												    	<div class="row"><h1><p></p><br></h1></div>
											  		 	<div class="row" id="heatmap_img" >  		 		
											  		 		<div class="col-md-12" style="height: 2000px" >  		 			 
											  		 			<iframe src='{{$current_contents[0]->PageURL}}' style="width: 100%;height: 100% ;border 1px solid gray">  		  				
											  		 			</iframe>
															</div>
														</div>								        
												    </div>
												    <!-- /.modal-content -->
												</div>
											</div>
										</div>
									    <!-- /.modal-dialog -->
									</div>
								</div>
							</div>
						</section>
					</div>
					 
				</div>
			</div>
		</div>
	</div>
</section>
</div>
</div>

<script src="{{URL::to('/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script src="{{URL::to('/plugins/chartjs/areachart/jquery.jqChart.min.js')}}" type="text/javascript"></script>

<script src="{{URL::to('/plugins/flot/jquery.flot.min.js')}}"></script>
<script src="{{URL::to('/plugins/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{URL::to('/plugins/flot/jquery.flot.pie.min.js')}}"></script>

<script src="{{URL::to('plugins/chartjs/Chart.min.js')}}"></script>

<script type="text/javascript">

  var alldata=[];

</script>

@foreach ($topentrys as $topentry)
<script type="text/javascript">
	alldata.push(['{{substr($topentry->Date,5)}}', '{{$topentry->domainname}}',{{$topentry->Value}}]);
</script>
@endforeach

<script type="text/javascript">

	var entrydate=[];
</script>

@foreach ($dategroup as $dtgroup)
<script type="text/javascript">
   entrydate.push('{{substr($dtgroup->Date,5)}}');
</script>
@endforeach
<script type="text/javascript">
  /* entrydate.splice(0,entrydate.length-7);
   var i=alldata.length-1;
   while (alldata[i][0]>=entrydate[0])
   {
   	  	i--;
   }
   alldata.splice(0,i+1);
   
   var domain_len=0;
   var domains=[];
   for (i=0;i<alldata.length;i++)
   {
   		var re=0;
   		for (j=0;j<domain_len;j++)
   		{
   			if (alldata[i][1]==domains[j]) {re=1;break}
   		}		
   		if (re==0) {domains.push(alldata[i][1]);domain_len++;}
   }
   var series_data=[];
   for (i=0;i<domains.length;i++)
   {
   		var valuedata=[];
   		for (y=0;y<entrydate.length;y++)
   		{
   			var vlu=0;	
   			for (j=0;j<alldata.length;j++)
   			{
				if(entrydate[y]==alldata[j][0] && domains[i]==alldata[j][1])					
				{
					vlu=alldata[j][2];break;
				}	

   			}
   			valuedata.push(vlu);

   		}

   		series_data.push({
                        type: 'column',
                        title: domains[i],
                        data: valuedata
                    });

   }*/
</script>


<script type="text/javascript">
	var dt10=[];	var dt20=[];	var dt30=[];	var dt40=[];
	var entry_donut={
		'dount_google':'{{$dount_google}}',
		'dount_bing':'{{$dount_bing}}',
		'dount_yahoo':'{{$dount_yahoo}}',
		'dount_ask':'{{$dount_ask}}',
		'dount_dogpile':'{{$dount_dogpile}}'
	};
	 
</script>
<?php /* 
@foreach ($current_contents as $current_content)
<script type="text/javascript">
	var cb=['{{substr($current_content->Date,5)}}',{{$current_content->Value}}];
	dt10.push(cb);
	dt20.push(cb);
	dt30.push(cb);
	dt40.push(cb);
	var pg='{{substr($current_content->PageName,-20)}}';
	
</script>
@endforeach
*/ ?>

<script src="{{URL::to('/js/ContentDetail.js')}}" type="text/javascript"></script>

<script type="text/javascript">
/*	selectDays();*/
var setedHeatmap=true;

  function onheatmap(){
    if (setedHeatmap){
      $("#heat_btn").css({"background":"url('img/heatmap1.png')"});
       
      setedHeatmap=false;
    } 
    else{
      $("#heat_btn").css({"background":"url('img/heatmap0.png')"});
       
      setedHeatmap=true;
    } 
  }


function performance(days=21)
{ 
	$.get("{{URL::to('/Content/performance')}}",{days:days,PageName:"{{$PageName}}"}).done(function( data ) {

		/*[{
	                type: 'area',
	                title: 'Sessions',
	                color:'red',
	                fillStyle: '#2d69a0',
	                data: [['Jan22', 0], ['Jan23', 30], ['Jan24', 70],
	                       ['Jan25', 60], ['Jan26', 65], ['Jan27', 80], ['Jan28', 90]]
	      }]*/
		$('#jqChart').jqChart({
	        title: { text: '' },
	        animation: { duration: 1 },
	        series: data.jqChart
	    });

		$('#P_Score').html(data.Trending);
	    $('#P_Prospects').html(data.ProspectsGenerated);

	    
	});
}
function engagement(days)
{ 
	$.get("{{URL::to('/Content/engagement')}}",{days:days,PageName:"{{$PageName}}"}).done(function( data ) {

		$('#E_AwarenessDetails').html('<i class="fa fa-arrow-'+data.AwarenessFlag+' fa-4x white-trend" aria-hidden="true">'+data.AwarenessValue+'</i>');
	    $('#E_EngagementDetails').html('<i class="fa fa-arrow-'+data.EngagementFlag+' fa-4x white-trend" aria-hidden="true">'+data.EngagementValue+'</i>');
	    $('#E_ConsiderationDetails').html('<i class="fa fa-arrow-'+data.ConsiderationFlag+' fa-4x white-trend" aria-hidden="true">'+data.ConsiderationValue+'</i>');
	    $('#E_ConvertedDetails').html('<i class="fa fa-arrow-'+data.ConvertedFlag+' fa-4x white-trend" aria-hidden="true">'+data.ConvertedValue+'</i>');
	    
	});
}

function topEntryPoints(months=6)
{
	$.get("{{URL::to('/Content/topEntryPoints')}}",{months:months,PageName:"{{$PageName}}"}).done(function( data ) {

		// Get context with jQuery - using jQuery's .get() method.
		  var pieChartCanvas = $("#donut-chart").get(0).getContext("2d");
		  var pieChart = new Chart(pieChartCanvas);

		  var PieData = data.pieChartPoints;
		 
		  var pieOptions = {
		    segmentShowStroke: true,
		    segmentStrokeColor: "#fff",
		    segmentStrokeWidth: 1,
		    percentageInnerCutout: 50, // This is 0 for Pie charts
		    animationSteps: 100,
		    animationEasing: "easeOutBounce",
		    animateRotate: true,
		    animateScale: false,
		    responsive: true,
		    maintainAspectRatio: false,
		    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
		    tooltipTemplate: "<%=value %> <%=label%> users"
		  };
		  pieChart.Doughnut(PieData, pieOptions);

		  //$('#jqChart_bar').empty();
		  $('#jqChart_bar').jqChart({
                title: { text: '' },
                legend: { location: 'top' }, // { visible: false },//
                animation: { duration: 1 },
                border: { visible: false },
                axes: data.barChartAxes/*[
                    {
                        type: 'category',
                        location: 'bottom',
                        categories: ['Oct', 'Nov', 'Dec', 'Jan','Feb','Mar','Apr']
                    }
                ]*/,
                series: data.barChartPoints/*[
                    {
                        type: 'column',
                        title: 'Google',
                        fillStyle: '#31ca6a',
                        data: [62, 70, 68, 58,23,45,54]
                    },
                    {
                        type: 'column',
                        title: 'Bing',
                        fillStyle: '#327aba',
                        data: [56, 50, 62, 65,45,32,64]
                    },
                    {
                        type: 'column',
                        title: 'Yahoo',
                        fillStyle: '#e70047',
                        data: [60, 55, 42, 68,45,23,64]
                    },
                    {
                        type: 'column',
                        title: 'Ask',
                        fillStyle: '#272a52',
                        data: [68, 30, 56, 40,34,56,65]
                    },
                    {
                        type: 'column',
                        title: 'dogpile',
                        fillStyle: '#f5f7ff',
                        data: [68, 30, 56, 40,34,56,65]
                    }

                ]*/
            });
	});

}

$( document ).ready(function() {

  performance(); 
  topEntryPoints();

});
</script>

@endsection



