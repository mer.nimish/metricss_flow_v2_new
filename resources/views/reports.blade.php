@extends('template.template')

@section('content-header')
<link rel="stylesheet" href="{{URL::to('/css/report.css')}}">
<style type="text/css">
  .title_back_grey{
    background-color:#f5f7ff; 
   }
   .bar_avg_visitor{
      height: 30px;border:1px solid black;
      box-shadow: 1px 1px grey;
      padding: 0;
      margin-top: 10px;margin-bottom: 10px;
   }
   svg{
    background-color: white !important
   }
</style>
<section class="content-header">
  <div id="breadcrumbs">
    <h4 class="bold"><i class="fa fa-chevron-left" aria-hidden="true"></i><br>
      </h4>
  </div> 
  <div id="client_logo">
    <img src="img/{{Session('client_id')}}.png" alt="client_logo">
  </div>
</section>
@endsection

@section('content')
 
<section id="CurrentView">
  <div class="row">
    <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h3 class="bold">Current View</h3> 
    </div>
    </div>
    <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12" style="padding: 30px;">
      
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 1px solid #35395d;padding: 8px;">
          <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                <div class="form-group">
                  <label>Date Range</label>
                  <select class="form-control">
                      <option>Last 30 Days</option>
                      <option>Last 60 Days</option>
                      <option>Last 90 Days</option>
                      <option>Last 10 Days</option>
                      <option>Last 20 Days</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="col-lg-4 col-md-4 col-sm-4  col-xs-12">
                <div class="form-group">
                  <label for="exampleFormControlInput1">Campaigns</label>
                  <input type="Campaigns" class="form-control" placeholder="All Campaigns">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="form-group">
                  <label for="exampleFormControlInput1">Channels</label>
                  <input type="Channels" class="form-control" placeholder="All Channels">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="form-group">
                  <label for="exampleFormControlInput1">Content</label>
                  <input type="Content" class="form-control" placeholder="All Content">
                </div>
              </div>
             </div>

          </div>
                 
      </div>
    </div>
  </div>
</section>

 

<section id="MounthlyConversionReport">
<div class="row">
<div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3 class="bold">Mounthly Conversion Report</h3>
  </div>
</div>      
  <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="alert-modal">
      <div class="modal">
        <div class="row">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header light-blue">
                    <div class="col-md-12 box1">
                      <div id="jqChart" style="width:95%; height: 250px; margin-left: 20px;">
                          <div class="col-xs-4 col-sm-4 col-lg-2 col-sm-4 text-right" style="color: #59f441">
                              <p class="c_score"><strong id="P_Score">8.6%</strong></p>
                            </div>     
                            <div class="col-xs-8 col-sm-8 col-lg-3 col-sm-8">
                               <p class="c_trending"><strong>TRENDING UP</strong></p>
                               <p class="c_prospects">Prospects Generated: <span id="P_Prospects">825</span></p>
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12 col-md-offset-1"> 
                      
                      <div class="col-md-2">
                        <i class="fa fa-stop fa-1x  labeltext" style="color:#327aba; margin-top: 5px; "></i> Total Visitors
                      </div>
                      <div class="col-md-4">
                        <i class="fa fa-stop fa-1x  labeltext" style="color:#272a52; margin-top: 5px; "></i> Total Conversions
                      </div>
                    </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
</section>

<section id="TOPchannels">
  <div class="row">
    <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="alert-modal">
          <div class="modal">
            <div class="modal-dialog alertbox">
                <div class="modal-content">
                    <div class="modal-header title_back_grey">         
                        <h4 class="modal-title">Lead Qualification</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                          <div id="donut-chart1"></div>
                      </div>
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#31ca6a; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>MCL's</h4>
                          </div>
                      </div>
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#327aba; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>MQL's</h4>
                          </div>
                      </div> 
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#fc5d56; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>SAL's</h4>
                          </div>
                      </div> 
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#e70047; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>SQL's</h4>
                          </div>
                      </div>                  
                    </div>
                </div>
            <!-- /.modal-content -->
            </div>
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>



      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="alert-modal">
          <div class="modal">
            <div class="modal-dialog alertbox">
                <div class="modal-content">
                    <div class="modal-header title_back_grey">         
                        <h4 class="modal-title">Source Breakdown</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                          <div id="donut-chart2"></div>
                      </div>
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-2x labeltext" style="color:#31ca6a; margin-top: 5px; "><br></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4><br></h4>
                          </div>
                      </div>
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#327aba; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>Organic Traffic </h4>
                          </div>
                      </div> 
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#fc5d56; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>Referral Traffic</h4>
                          </div>
                      </div> 
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#31ca6a; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>Direct URLs</h4>
                          </div>
                      </div>                  
                    </div>
                </div>
            <!-- /.modal-content -->
            </div>
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>



      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="alert-modal">
          <div class="modal">
            <div class="modal-dialog alertbox">
                <div class="modal-content">
                    <div class="modal-header title_back_grey">         
                        <h4 class="modal-title">Avg Visitor Breakdown</h4>
                    </div>
                    <div class="modal-body">
                      <div class="col-md-12 col-sm-12 col-xs-12 bar_avg_visitor"> 
                        <div style="width:100%;height:100%;background-color: #272a52;"></div>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 bar_avg_visitor"> 
                        <div style="width:80%;height:100%;background-color: #327aba;"></div>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 bar_avg_visitor"> 
                        <div style="width:40%;height:100%;background-color: #1668b9;"></div>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 bar_avg_visitor"> 
                        <div style="width:10%;height:100%;background-color: #7c9ebb;"></div>
                      </div>
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#272a52; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>Total Visitors</h4>
                          </div>
                      </div>
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#327aba; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>Engaged</h4>
                          </div>
                      </div> 
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#1668b9; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>Called to Action</h4>
                          </div>
                      </div> 
                      <div class="row">
                          <div class="float col-xs-2">
                            <i class="fa fa-stop fa-2x labeltext" style="color:#7c9ebb; margin-top: 5px; "></i> 
                          </div>
                          <div class="labeltext col-xs-10">
                            <h4>Converted</h4>
                          </div>
                      </div>                  
                    </div>
                </div>
            <!-- /.modal-content -->
            </div>
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>


      
    </div>
  </div>
</section>

<section id="LeadProgression">
<div class="row">
      <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3 class="bold">Lead Progression</h3>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <select class="form-control tableselect" onchange="topCampaigns(this.value);">
                  <option value="7">Last 7 Days</option>
                  <option value="14">Last 14 Days</option>
                  <option value="21">Last 21 Days</option>
                  <option value="30">Last 30 Days</option>
              </select>
         </div>     
    </div>
      <div class="row">
        <div class="col-lg-11 col-md-12 col-lg-12 col-xs-12">
          <section id="Engagement">           
            <div class="col-lg-10 col-sm-10 col-xs-10 col-md-10 center">
                <div class="col-md-12 box1">        
                    <div class="row" id="action_item_content" >
                        <div class="col-md-12">
                          <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10" align="center">
                                    <!-- <img class="img-responsive" src="/img/cluster2.png" style="text-align: center;">  -->
                                    <div id="bubble1" style="height: 200px;overflow: none;fill: blue;"></div>                    
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                  <p style="font-size: 30px;text-align: center;">60%</p>
                                  <p style="font-size: 20px;text-align: center;">Aware</p>
                                </div>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-6 col-xs-12">
                              <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10" align="center">
                                    <div id="bubble2" style="height: 200px;fill: red;"></div>                    
                                </div>
                              </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                  <p style="font-size: 30px;text-align: center;">100%</p>
                                  <p style="font-size: 20px;text-align: center;">Engagement</p>
                              </div>
                          </div>
                          </div>
                          <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="row">
                              <div class="col-md-1"></div>
                              <div class="col-md-10" align="center">
                                <div id="bubble3"  style="height: 200px;margin:auto;fill: green;"></div>                    
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-1"></div>
                              <div class="col-md-10">
                                <p style="font-size: 30px;text-align: center;">30%</p>
                                <p style="font-size: 20px;text-align: center;">Converted</p>
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>        
                  </div>
                </div>
              </section>
            
          </div>
          </div>
      </div>
</section>

<section id="Visitors">
<div class="row">
  <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h3 class="bold">Visitors Report</h3>
     </div>
  </div>
  <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="alert-modal">
      <div class="modal">
        <div class="row">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header ">
                    <div">
                      <div class="padding15 display col-xs-2 col-md-offset-1" style="color: blue">
                        <h1><strong><i class="fa fa-arrow-up  green-trend" aria-hidden="true"></i> 8%</strong></h1>
                      </div>     
                      <div class="padding15 display col-xs-3">
                         <p>Visits: 38,490</p>
                         <p>Referrals: 15,678</p>
                         <p>Organic: 32,721</p>
                      </div>
                    </div>
                    <div id="worldmap" style="margin-top: -100px;">
                    </div>
                  </div>
              </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>



<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/plugins/chartjs/areachart/jquery.jqChart.min.js" type="text/javascript"></script>



<script type="text/javascript">
  
  $('#jqChart').jqChart({
                title: { text: '' },
                animation: { duration: 1 },
                series: [
                    
                    {
                        type: 'area',
                        title: '',
                        fillStyle: '#327aba',
                        data: [['Oct', 40], ['Nov', 20], ['Dev', 50],
                               ['Jan', 45], ['Feb', 55], ['Mar', 75], ['Apr', 60]]
                    },
                   
                    {
                        type: 'area',
                        title: '',
                        fillStyle: '#272a52',
                        data: [['Oct', 30], ['Nov', 10], ['Dev', 20],
                               ['Jan', 15], ['Fev', 40], ['Mar', 33], ['Apr', 50]]
                    }
                ]
            });
</script>  
  
  

<script src="/plugins/flot/jquery.flot.min.js"></script>
<script src="/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="/js/donut_report.js"></script>


<script type="text/javascript" src="/plugins/chartjs/d3.min.js"></script>

<script type="text/javascript">
  
(function() {
  
  var bubble_count=6;
  // Fake JSON data
  var json;
  if (bubble_count==1)  json={"countries_msg_vol": {
    "A": 9 }};
  if (bubble_count==2)  json={"countries_msg_vol": {
    "A": 9, "B": 4 }};
  if (bubble_count==3)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3  }};
  if (bubble_count==4)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2}};
  if (bubble_count==5)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1 }};
  if (bubble_count==6)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6}};
  if (bubble_count==7)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4}};
  if (bubble_count==8)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4, "H": 2   }};
  if (bubble_count==9)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4, "H": 2   }};
  if (bubble_count==10)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4, "H": 2   }};
  if (bubble_count==0)  json={"countries_msg_vol": {
    }};

  
    
  
  // D3 Bubble Chart 

  var diameter = 200/10*bubble_count;
    diameter=diameter.toFixed(0);

  var svg = d3.select('#bubble1').append('svg')
          .attr('id','sg1')
          .attr('width', diameter)
          .attr('height', diameter);

  var bubble = d3.layout.pack()
        .size([diameter, diameter])
        .value(function(d) {return d.size;})
         // .sort(function(a, b) {
        //  return -(a.value - b.value)
        // }) 
        .padding(3);
  
  // generate data with calculated layout values
  var nodes = bubble.nodes(processData(json))
            .filter(function(d) { return !d.children; }); // filter out the outer bubble
 
  var vis = svg.selectAll('circle')
          .data(nodes);
  
  vis.enter().append('circle')
      .attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; })
      .attr('r', function(d) { return d.r; })
      .attr('class', function(d) { return d.className; });
  
  function processData(data) {
    var obj = data.countries_msg_vol;

    var newDataSet = [];

    for(var prop in obj) {
      newDataSet.push({name: prop, className: prop.toLowerCase(), size: obj[prop]});
    }
    return {children: newDataSet};
  }
  document.getElementById("sg1").style.marginTop  =(200-diameter)/2+'px' ; 
})();
</script>

<script type="text/javascript">
  
(function() {

  // Fake JSON data
 
  var bubble_count=10;
  // Fake JSON data
  var json;
  if (bubble_count==1)  json={"countries_msg_vol": {
    "A": 9 }};
  if (bubble_count==2)  json={"countries_msg_vol": {
    "A": 9, "B": 4 }};
  if (bubble_count==3)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3  }};
  if (bubble_count==4)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2}};
  if (bubble_count==5)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1 }};
  if (bubble_count==6)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6}};
  if (bubble_count==7)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4}};
  if (bubble_count==8)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4, "H": 2   }};
  if (bubble_count==9)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4, "H": 2   }};
  if (bubble_count==10)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4, "H": 2   }};
  if (bubble_count==0)  json={"countries_msg_vol": {
    }};
  
  // D3 Bubble Chart 

  var diameter = 200/10*bubble_count;
    diameter=diameter.toFixed(0);

  var svg = d3.select('#bubble2').append('svg')
          .attr('id','sg2')
          .attr('width', diameter)
          .attr('height', diameter);

  var bubble = d3.layout.pack()
        .size([diameter, diameter])
        .value(function(d) {return d.size;})
         // .sort(function(a, b) {
        //  return -(a.value - b.value)
        // }) 
        .padding(3);
  
  // generate data with calculated layout values
  var nodes = bubble.nodes(processData(json))
            .filter(function(d) { return !d.children; }); // filter out the outer bubble
 
  var vis = svg.selectAll('circle')
          .data(nodes);
  
  vis.enter().append('circle')
      .attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; })
      .attr('r', function(d) { return d.r; })
      .attr('class', function(d) { return d.className; });
  
  function processData(data) {
    var obj = data.countries_msg_vol;

    var newDataSet = [];

    for(var prop in obj) {
      newDataSet.push({name: prop, className: prop.toLowerCase(), size: obj[prop]});
    }
    return {children: newDataSet};
  }
  document.getElementById("sg2").style.marginTop  =(200-diameter)/2+'px' ; 
})();
</script>

<script type="text/javascript">
  
(function() {

  // Fake JSON data
 
  var bubble_count=3;
  // Fake JSON data
  var json;
  if (bubble_count==1)  json={"countries_msg_vol": {
    "A": 9 }};
  if (bubble_count==2)  json={"countries_msg_vol": {
    "A": 9, "B": 4 }};
  if (bubble_count==3)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3  }};
  if (bubble_count==4)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2}};
  if (bubble_count==5)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1 }};
  if (bubble_count==6)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6}};
  if (bubble_count==7)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4}};
  if (bubble_count==8)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4, "H": 2   }};
  if (bubble_count==9)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4, "H": 2   }};
  if (bubble_count==10)  json={"countries_msg_vol": {
    "A": 9, "B": 4, "C": 3, "D": 2, "E": 1, "F": 6, "G": 4, "H": 2   }};
  if (bubble_count==0)  json={"countries_msg_vol": {
    }};


  
  // D3 Bubble Chart 

  var diameter = 200/10*bubble_count;
    diameter=diameter.toFixed(0);

  var svg = d3.select('#bubble3').append('svg')
          .attr('id','sg3') 
          .attr('width', diameter)
          .attr('height', diameter+'px');

  var bubble = d3.layout.pack()
        .size([diameter, diameter])
        .value(function(d) {return d.size;})
         // .sort(function(a, b) {
        //  return -(a.value - b.value)
        // }) 
        .padding(3);
  
  // generate data with calculated layout values
  var nodes = bubble.nodes(processData(json))
            .filter(function(d) { return !d.children; }); // filter out the outer bubble
 
  var vis = svg.selectAll('circle')
          .data(nodes);
  
  vis.enter().append('circle')
      .attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; })
      .attr('r', function(d) { return d.r; })
      .attr('class', function(d) { return d.className; });
  
  function processData(data) {
    var obj = data.countries_msg_vol;

    var newDataSet = [];

    for(var prop in obj) {
      newDataSet.push({name: prop, className: prop.toLowerCase(), size: obj[prop]});
    }
    return {children: newDataSet};
  }
  
   document.getElementById("sg3").style.marginTop  =(200-diameter)/2+'px' ;
  

})();


</script>


<script src="/dist/jquery.twism.js"></script>
<script>
     
        $('#worldmap').twism("create", 
        {
          map: "world",
          border: "red",
          disableCountries: ['ar'],
          individualCountrySettings: [{
            name: "cn",
            color: "red"
          }]
        }, function() {
          //callback function after map loads
          $('#worldmap').twism("setCountry", {
            name: "ca",
            color: "blue",
            hoverColor: "yellow",
            hoverBorder: "purple",
          });
        });
     
</script>

@endsection