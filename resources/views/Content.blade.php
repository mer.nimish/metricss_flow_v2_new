@extends('template.template')

@section('content-header')
<style type="text/css">
	.top_content_title a{
		line-height: 35px;
	}
	.top_content_title{
		height: 60px;
	}

</style>
<section class="content-header">
	 
	<div id="client_logo">
		<img src="img/{{$client_id}}.png" alt="client_logo"  >
	</div>
</section>
@endsection
@section('content')

<div class="main-content">
	<div class="row">
		<section id="ContentSearch">
			<div class="row">
				<div class="col-md-12 col-lg-11 col-xs-12">
					<h3 class="bold">Content Search</h3>
					<div class="alert-modal">
						<div class="modal">
							<div class="col-md-12 col-xs-12 col-sm-12">
								<div class="row">
							    	<div class="box">
				                      <div class="table-box">
				                        <table id="example1" class="table table-hover">
				                            <thead>
				                              	<tr id="tablehead">
				                                
					                                <th>Latest Content</th>
					                                <th >Score</th>
					                                <th >Prospects</th>
					                                <th >Date Posted</th>
				                              	</tr>
				                            </thead>
				                            <tbody>
				                            	@foreach ($contents as $content)
				                            	<tr>
					                                <td><a href='/ContentDetail?PageName={{$content->PageName}}&Date={{$content->Date}}'>{{$content->PageName}}</a></td>
					                                <td>{{number_format($content->score,2)}}</td>
					                                <td>{{$content->Value}}</td>
					                                <td>{{date("M d, Y",strtotime($content->Date))}}</td>
					                            </tr>
					                            @endforeach
				                            
				                            </tbody>
				                          </table>
				                      </div>
				                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="TopPerformingContent">
			<div class="row">
				<div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
					<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
						<h3 class="bold">Top Performing Content</h3>
					</div>
					<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
						<select class="form-control tableselect" style="border:none;" onchange="topPerformingContent(this.value)">
							<option value="30" selected>Last 30 Days</option>
							<option value="60">Last 60 Days</option>
							<option value="90">Last 90 Days</option>
						</select>
					</div>
				</div>
				<div class="col-md-12 col-lg-11 col-xs-12">
					
					<div class="alert-modal">
						<div class="modal">
							<div class="col-md-12 col-xs-12 col-sm-12">
								<div class="modal-dialog">
								    <div class="modal-content">
								        <div class="modal-header light-blue">
								        	
								        	<div class="nav-tabs-custom light-blue" >
								                <ul class="nav nav-tabs ">
									                  <li class="active"><a href="#tab_1" data-toggle="tab"><h3>Awareness</h3></a></li>
									                  <li><a href="#tab_2" data-toggle="tab"><h3>Engagement</h3></a></li>
									                  <li><a href="#tab_3" data-toggle="tab"><h3>Consideration</h3></a></li>
									                  <li><a href="#tab_4" data-toggle="tab"><h3>Conversion</h3></a></li>
								                </ul>
								                <div class="tab-content light-blue">
                  									<div class="tab-pane active" id="tab_1">

                  										<div class="row">
                  											<div class="col-xs-1 col-md-1 col-lg-1 col-sm-1">
                  												<br><br><br><br><br><br><br>
                  												<a onclick="AwarenessPlusDivs(-1)" class="float-right"><h1><i class="fa fa-chevron-left" aria-hidden="true"></i></h1></a>
                  											</div>


                  											<div id="AwarenessData" class="w3-content w3-display-container">
															  <div class="AwarenessSlides" style="width:100%;text-align: center;">
															  	<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">
                  												@foreach ($top_contents_Awareness as $key => $content)
                  												<div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
																	<div class="alert-modal">
																		<div class="modal">
																			<div class="modal-dialog alertbox">
																			    <div class="modal-content">
																			        <div class="modal-header blue top_content_title">         
																			            <h4 class="modal-title"><a href="/ContentDetail?PageName={{$content->PageName}}&Date={{$content->Date}}"  style="color: white">{{$content->PageName}}</a></h4>
																			        </div>
																			        <div class="modal-body">
																			        	<div class="row ">
																				            <div class="col-md-12">
																				            	<div class="alert-label">
																				            		<div class="box-body">
																					            		<div class="thumbnail_image">         
																					                   		<a href="/ContentDetail?PageName={{$content->PageName}}&Date={{$content->Date}}">
																					                      		<div id="screenshot" style="overflow: hidden; border: 1px solid black">                           
																					                          		<!-- <iframe sandbox="allow-pointer-lock" scrolling="no" src='http://m5.ca/' style="border:none;"></iframe>  -->
																					                          		<iframe src="{{$content->PageURL}}" name= "tabsa" width="95%" height="100%" frameborder="0" allowtransparency="true" style="height: 250px;"></iframe>                      
																					                         		
																					                       		</div>
																					                   		</a>
																					                	</div>
																				                	</div>
																							    </div>
																							    <div class="col-md-12 col-sm-12 col-xs-12">
																					            	 <div class="col-lg-3" style="padding: 0px;">
																					            	 	<h1  style="margin: 0px; color:#7CFC00"><strong>{{$content->avgscore}}</strong></h1>
	
																					            	 </div>								            			
																					            	 <div class="col-lg-9">
																					            	 	<p>Prospects Generated: {{$content->sum,1}} </p>
																					            	 	<p>Posted: {{$content->Date}}</p>
	
																					            	 </div>								            			
																					            </div>
																							</div>
																						</div>			           
																					</div>
																				</div>
																			<!-- /.modal-content -->
																			</div>
																		</div>
																		<!-- /.modal-dialog -->
																	</div>
																</div>

																	@if(($key+1) % 2 == 0)
																		@if(($key+1) != count($top_contents_Awareness))
																			</div></div>
																  			<div class="AwarenessSlides" style="width:100%;text-align: center;"><div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">
																  	    @endif
																	@endif
																@endforeach
                  											</div>
															</div>
															</div>

                  											<div class="col-md-1 col-sm-1 col-lg-1 col-xs-1">
                  												<br><br><br><br><br><br><br>
                  												<a onclick="AwarenessPlusDivs(1)"><h1><i class="fa fa-chevron-right" aria-hidden="true"></i></h1></a>
                  											</div>
                  										</div>
                  									</div>
                  									<div class="tab-pane" id="tab_2">
                  										<div class="row">
                  											<div class="col-md-1 col-sm-1 col-lg-1 col-xs-1">
                  												<br><br><br><br><br><br><br>
                  												<a onclick="EngagementPlusDivs(-1)" class="float-right"><h1><i class="fa fa-chevron-left" aria-hidden="true"></i></h1></a>
                  											</div>
                  											

                  											<div id="EngagementData" class="w3-content w3-display-container">
															  <div class="EngagementSlides" style="width:100%;text-align: center;">
															  	<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">
                  												@foreach ($top_contents_Engagement as $key => $content)
                  												<div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
																	<div class="alert-modal">
																		<div class="modal">
																			<div class="modal-dialog alertbox">
																			    <div class="modal-content">
																			        <div class="modal-header blue top_content_title">         
																			            <h4 class="modal-title"><a href="/ContentDetail?PageName={{$content->PageName}}&Date={{$content->Date}}"  style="color: white">{{$content->PageName}}</a></h4>
																			        </div>
																			        <div class="modal-body">
																			        	<div class="row ">
																				            <div class="col-md-12">
																				            	<div class="alert-label">
																				            		<div class="box-body">
																					            		<div class="thumbnail_image">         
																					                   		<a href="/ContentDetail?PageName={{$content->PageName}}&Date={{$content->Date}}">
																					                      		<div id="screenshot" style="overflow: hidden; border: 1px solid black">                           
																					                          		<!-- <iframe sandbox="allow-pointer-lock" scrolling="no" src='http://m5.ca/' style="border:none;"></iframe>  -->
																					                          		<iframe src="{{$content->PageURL}}" name= "tabsa" width="95%" height="100%" frameborder="0" allowtransparency="true" style="height: 250px;"></iframe>                      
																					                         		
																					                       		</div>
																					                   		</a>
																					                	</div>
																				                	</div>
																							    </div>
																							    <div class="col-md-12 col-sm-12 col-xs-12">
																					            	 <div class="col-lg-3" style="padding: 0px;">
																					            	 	<h1  style="margin: 0px; color:#7CFC00"><strong>{{$content->avgscore}}</strong></h1>
	
																					            	 </div>								            			
																					            	 <div class="col-lg-9">
																					            	 	<p>Prospects Generated: {{$content->sum,1}} </p>
																					            	 	<p>Posted: {{$content->Date}}</p>
	
																					            	 </div>								            			
																					            </div>
																							</div>
																						</div>			           
																					</div>
																				</div>
																			<!-- /.modal-content -->
																			</div>
																		</div>
																		<!-- /.modal-dialog -->
																	</div>
																</div>

																	@if(($key+1) % 2 == 0)
																		@if(($key+1) != count($top_contents_Engagement))
																			</div></div>
																  			<div class="EngagementSlides" style="width:100%;text-align: center;"><div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">
																  	    @endif
																	@endif
																@endforeach
                  											</div>
															</div>
															</div>

                  											<div class="col-md-1 col-sm-1 col-lg-1 col-xs-1">
                  												<br><br><br><br><br><br><br>
                  												<a onclick="EngagementPlusDivs(1)"><h1><i class="fa fa-chevron-right" aria-hidden="true"></i></h1></a>
                  											</div>
                  										</div>
                  									</div>
                  									<div class="tab-pane" id="tab_3">
                  										<div class="row">
                  											<div class="col-md-1 col-sm-1 col-lg-1 col-xs-1">
                  												<br><br><br><br><br><br><br>
                  												<a onclick="ConsiderationPlusDivs(-1)" class="float-right"><h1><i class="fa fa-chevron-left" aria-hidden="true"></i></h1></a>
                  											</div>

                  											<div id="ConsiderationData" class="w3-content w3-display-container">
															  <div class="ConsiderationSlides" style="width:100%;text-align: center;">
															  	<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">
                  												@foreach ($top_contents_Consideration as $key => $content)
                  												<div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
																	<div class="alert-modal">
																		<div class="modal">
																			<div class="modal-dialog alertbox">
																			    <div class="modal-content">
																			        <div class="modal-header blue top_content_title">         
																			            <h4 class="modal-title"><a href="/ContentDetail?PageName={{$content->PageName}}&Date={{$content->Date}}"  style="color: white">{{$content->PageName}}</a></h4>
																			        </div>
																			        <div class="modal-body">
																			        	<div class="row ">
																				            <div class="col-md-12">
																				            	<div class="alert-label">
																				            		<div class="box-body">
																					            		<div class="thumbnail_image">         
																					                   		<a href="/ContentDetail?PageName={{$content->PageName}}&Date={{$content->Date}}">
																					                      		<div id="screenshot" style="overflow: hidden; border: 1px solid black">                           
																					                          		<!-- <iframe sandbox="allow-pointer-lock" scrolling="no" src='http://m5.ca/' style="border:none;"></iframe>  -->
																					                          		<iframe src="{{$content->PageURL}}" name= "tabsa" width="95%" height="100%" frameborder="0" allowtransparency="true" style="height: 250px;"></iframe>                      
																					                         		
																					                       		</div>
																					                   		</a>
																					                	</div>
																				                	</div>
																							    </div>
																							    <div class="col-md-12 col-sm-12 col-xs-12">
																					            	 <div class="col-lg-3" style="padding: 0px;">
																					            	 	<h1  style="margin: 0px; color:#7CFC00"><strong>{{$content->avgscore}}</strong></h1>
	
																					            	 </div>								            			
																					            	 <div class="col-lg-9">
																					            	 	<p>Prospects Generated: {{$content->sum,1}} </p>
																					            	 	<p>Posted: {{$content->Date}}</p>
	
																					            	 </div>								            			
																					            </div>
																							</div>
																						</div>			           
																					</div>
																				</div>
																			<!-- /.modal-content -->
																			</div>
																		</div>
																		<!-- /.modal-dialog -->
																	</div>
																</div>

																	@if(($key+1) % 2 == 0)
																		@if(($key+1) != count($top_contents_Consideration))
																			</div></div>
																  			<div class="ConsiderationSlides" style="width:100%;text-align: center;"><div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">
																  	    @endif
																	@endif
																@endforeach
                  											</div>
															</div>
															</div>
                  										
                  											<div class="col-md-1 col-sm-1 col-lg-1 col-xs-1">
                  												<br><br><br><br><br><br><br>
                  												<a onclick="ConsiderationPlusDivs(1)"><h1><i class="fa fa-chevron-right" aria-hidden="true"></i></h1></a>
                  											</div>
                  										</div>
                  									</div>
                  									<div class="tab-pane" id="tab_4">
                  										<div class="row">
                  											<div class="col-md-1 col-sm-1 col-lg-1 col-xs-1">
                  												<br><br><br><br><br><br><br>
                  												<a onclick="ConversionPlusDivs(-1)" class="float-right"><h1><i class="fa fa-chevron-left" aria-hidden="true"></i></h1></a>
                  											</div>

                  											<div id="ConversionData" class="w3-content w3-display-container">
															  <div class="ConversionSlides" style="width:100%;text-align: center;">
															  	<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">
                  												@foreach ($top_contents_Conversion as $key => $content)
                  												<div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
																	<div class="alert-modal">
																		<div class="modal">
																			<div class="modal-dialog alertbox">
																			    <div class="modal-content">
																			        <div class="modal-header blue top_content_title">         
																			            <h4 class="modal-title"><a href="/ContentDetail?PageName={{$content->PageName}}&Date={{$content->Date}}"  style="color: white">{{$content->PageName}}</a></h4>
																			        </div>
																			        <div class="modal-body">
																			        	<div class="row ">
																				            <div class="col-md-12">
																				            	<div class="alert-label">
																				            		<div class="box-body">
																					            		<div class="thumbnail_image">         
																					                   		<a href="/ContentDetail?PageName={{$content->PageName}}&Date={{$content->Date}}">
																					                      		<div id="screenshot" style="overflow: hidden; border: 1px solid black">                           
																					                          		<!-- <iframe sandbox="allow-pointer-lock" scrolling="no" src='http://m5.ca/' style="border:none;"></iframe>  -->
																					                          		<iframe src="{{$content->PageURL}}" name= "tabsa" width="95%" height="100%" frameborder="0" allowtransparency="true" style="height: 250px;"></iframe>                      
																					                         		
																					                       		</div>
																					                   		</a>
																					                	</div>
																				                	</div>
																							    </div>
																							    <div class="col-md-12 col-sm-12 col-xs-12">
																					            	 <div class="col-lg-3" style="padding: 0px;">
																					            	 	<h1  style="margin: 0px; color:#7CFC00"><strong>{{$content->avgscore}}</strong></h1>
	
																					            	 </div>								            			
																					            	 <div class="col-lg-9">
																					            	 	<p>Prospects Generated: {{$content->sum,1}} </p>
																					            	 	<p>Posted: {{$content->Date}}</p>
	
																					            	 </div>								            			
																					            </div>
																							</div>
																						</div>			           
																					</div>
																				</div>
																			<!-- /.modal-content -->
																			</div>
																		</div>
																		<!-- /.modal-dialog -->
																	</div>
																</div>

																	@if(($key+1) % 2 == 0)
																		@if(($key+1) != count($top_contents_Conversion))
																			</div></div>
																  			<div class="ConversionSlides" style="width:100%;text-align: center;"><div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">
																  	    @endif
																	@endif
																@endforeach
                  											</div>
															</div>
															</div>
                  											
                  											<div class="col-md-1 col-sm-1 col-lg-1 col-xs-1">
                  												<br><br><br><br><br><br><br>
                  												<a onclick="ConversionPlusDivs(1)"><h1><i class="fa fa-chevron-right" aria-hidden="true"></i></h1></a>
                  											</div>
                  										</div>
                  									</div>
                  								</div>
								            </div>

								            
								        </div>
								    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		
		<section id="TopKeywordsUsed">
			<div class="row">

				<div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
					<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
						<h3 class="bold">Top Keywords Used</h3>
					</div>
					<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
						<select class="form-control tableselect" style="border:none;" onchange="topKeywordsUsed(this.value)">
							<option value="30" selected>Last 30 Days</option>
							<option value="60">Last 60 Days</option>
							<option value="90">Last 90 Days</option>
						</select>
					</div>
				</div> 
				<div class="col-md-12 col-lg-11 col-xs-12">
					
					<div class="alert-modal">
						<div class="modal">
							<div class="col-md-12 col-xs-12 col-sm-12">

								<div class="modal-dialog">
								    <div class="modal-content">
								        <div class="modal-header light-blue">
								        	<div class="nav-tabs-custom light-blue" >
								                <ul class="nav nav-tabs ">
									                  <li class="active tab01"><a href="#tab_w1" data-toggle="tab"><h3>Awareness</h3></a></li>
									                  <li class="tab02"><a href="#tab_w2" data-toggle="tab"><h3>Engagement</h3></a></li>
									                  <li class="tab03"><a href="#tab_w3" data-toggle="tab"><h3>Consideration</h3></a></a></li>
									                  <li class="tab04"><a href="#tab_w4" data-toggle="tab"><h3>Conversion</h3></a></li>
								                </ul>
								                <div class="tab-content light-blue">
                  									<div class="tab-pane active" id="tab_w1">
                  										<div class="row">
                  											<div id="Word_chart1"></div>
                  										</div>

                  									</div>
                  									<div class="tab-pane" id="tab_w2">
                  										<div class="row">
                  											<div id="Word_chart2"></div>
                  										</div>
                  									</div>
                  									<div class="tab-pane" id="tab_w3">
                  										<div class="row">
                  											<div id="Word_chart3"></div>
                  										</div>
                  									</div>
                  									<div class="tab-pane" id="tab_w4">
                  										<div class="row">
                  											<div id="Word_chart4"></div>
                  										</div>
                  									</div>
                  								</div>
								            </div>
								        </div>
								    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/d3/3.4.11/d3.min.js"></script>
<script src="{{URL::to('/plugins/chartjs/d3.layout.cloud.js')}}"></script>
<script type="text/javascript">
	var text_string1="";
	var text_string2="";
	var text_string3="";
	var text_string4="";
</script>
@foreach ($top_contents_Awareness as $content)
<script type="text/javascript">
	text_string1+="{{$content->PageName}}";
</script>
@endforeach
<?php /* 
@foreach ($top_contents_Awareness as $content)
<script type="text/javascript">
	text_string1+="{{$content->PageName}}";
</script>
@endforeach
@foreach ($top_contents_Engagement as $content)
<script type="text/javascript">
	text_string2+="{{$content->PageName}}";
</script>
@endforeach
@foreach ($top_contents_Consideration as $content)
<script type="text/javascript">
	text_string3+="{{$content->PageName}}";
</script>
@endforeach
@foreach ($top_contents_Conversion as $content)
<script type="text/javascript">
	text_string4+="{{$content->PageName}}";
</script>
@endforeach
*/ ?>

<!--  <script src="{{URL::to('/js/Content.js')}}"></script>  -->






<script>

var slideIndex = 1;

AwarenessShowDivs(slideIndex);
function AwarenessPlusDivs(n) {
  AwarenessShowDivs(slideIndex += n);
}
function AwarenessShowDivs(n) {
  var i;
  var x = document.getElementsByClassName("AwarenessSlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}


EngagementShowDivs(slideIndex);
function EngagementPlusDivs(n) {
  EngagementShowDivs(slideIndex += n);
}
function EngagementShowDivs(n) {
  var i;
  var x = document.getElementsByClassName("EngagementSlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

ConsiderationShowDivs(slideIndex);
function ConsiderationPlusDivs(n) {
  ConsiderationShowDivs(slideIndex += n);
}
function ConsiderationShowDivs(n) {
  var i;
  var x = document.getElementsByClassName("ConsiderationSlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

ConversionShowDivs(slideIndex);
function ConversionPlusDivs(n) {
  ConversionShowDivs(slideIndex += n);
}
function ConversionShowDivs(n) {
  var i;
  var x = document.getElementsByClassName("ConversionSlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}




function topPerformingContent(days=21)
{ 
	$.get("{{URL::to('/Content/topPerformingContent')}}",{days:days}).done(function( data ) {

	var AwarenessData = '<div class="AwarenessSlides" style="width:100%;text-align: center;">'
		+	'<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">'; 

	$.each(data.AwarenessData, function( index, value ) {
 
		AwarenessData += '<div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">'
		+		'<div class="alert-modal">'
		+			'<div class="modal">'
		+				'<div class="modal-dialog alertbox">'
		+				    '<div class="modal-content">'
		+				        '<div class="modal-header blue top_content_title">'         
		+				            '<h4 class="modal-title"><a href="/ContentDetail?PageName='+value.PageName+'&Date='+value.Date+'"  style="color: white">'+value.PageName+'</a></h4>'
		+				        '</div>'
		+				        '<div class="modal-body">'
		+				        	'<div class="row ">'
		+					            '<div class="col-md-12">'
		+					            	'<div class="alert-label">'
		+					            		'<div class="box-body">'
		+						            		'<div class="thumbnail_image">'       
		+						                   		'<a href="/ContentDetail?PageName='+value.PageName+'&Date='+value.Date+'">'
		+						                      		'<div id="screenshot" style="overflow: hidden; border: 1px solid black">'  
		+						                          		'<iframe src="'+value.PageURL+'" name= "tabsa" width="95%" height="100%" frameborder="0" allowtransparency="true" style="height: 250px;"></iframe>'  
		+						                       		'</div>'
		+						                   		'</a>'
		+						                	'</div>'
		+					                	'</div>'
		+								    '</div>'
		+								    '<div class="col-md-12 col-sm-12 col-xs-12">'
		+						            	 '<div class="col-lg-3" style="padding: 0px;">'
		+						            	 	'<h1  style="margin: 0px; color:#7CFC00"><strong>'+value.avgscore+'</+strong></h1>'
		+						            	 '</div>'								            			
		+						            	 '<div class="col-lg-9">'
		+						            	 	'<p>Prospects Generated: '+value.sum+' </p>'
		+						            	 	'<p>Posted: '+value.Date+'</p>'
		+						            	 '</div>'						            			
		+						            '</div>'
		+								'</div>'
		+							'</div>'		           
		+						'</div>'
		+					'</div>'
		+				'</div>'
		+			'</div>'
		+		'</div>'
		+	'</div>';

		if((index+1) % 2 == 0)
		{ 
			if(data.AwarenessData.length != (index+1))
				AwarenessData += '</div></div><div class="AwarenessSlides" style="width:100%;text-align: center;"><div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">';
		}
	});

	AwarenessData += '</div>'
		+	'</div>';

	$('#AwarenessData').html(AwarenessData);

	AwarenessShowDivs(slideIndex);



	var EngagementData = '<div class="EngagementSlides" style="width:100%;text-align: center;">'
		+	'<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">'; 

	$.each(data.EngagementData, function( index, value ) {
 
		EngagementData += '<div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">'
		+		'<div class="alert-modal">'
		+			'<div class="modal">'
		+				'<div class="modal-dialog alertbox">'
		+				    '<div class="modal-content">'
		+				        '<div class="modal-header blue top_content_title">'         
		+				            '<h4 class="modal-title"><a href="/ContentDetail?PageName='+value.PageName+'&Date='+value.Date+'"  style="color: white">'+value.PageName+'</a></h4>'
		+				        '</div>'
		+				        '<div class="modal-body">'
		+				        	'<div class="row ">'
		+					            '<div class="col-md-12">'
		+					            	'<div class="alert-label">'
		+					            		'<div class="box-body">'
		+						            		'<div class="thumbnail_image">'       
		+						                   		'<a href="/ContentDetail?PageName='+value.PageName+'&Date='+value.Date+'">'
		+						                      		'<div id="screenshot" style="overflow: hidden; border: 1px solid black">'  
		+						                          		'<iframe src="'+value.PageURL+'" name= "tabsa" width="95%" height="100%" frameborder="0" allowtransparency="true" style="height: 250px;"></iframe>'  
		+						                       		'</div>'
		+						                   		'</a>'
		+						                	'</div>'
		+					                	'</div>'
		+								    '</div>'
		+								    '<div class="col-md-12 col-sm-12 col-xs-12">'
		+						            	 '<div class="col-lg-3" style="padding: 0px;">'
		+						            	 	'<h1  style="margin: 0px; color:#7CFC00"><strong>'+value.avgscore+'</+strong></h1>'
		+						            	 '</div>'								            			
		+						            	 '<div class="col-lg-9">'
		+						            	 	'<p>Prospects Generated: '+value.sum+' </p>'
		+						            	 	'<p>Posted: '+value.Date+'</p>'
		+						            	 '</div>'						            			
		+						            '</div>'
		+								'</div>'
		+							'</div>'		           
		+						'</div>'
		+					'</div>'
		+				'</div>'
		+			'</div>'
		+		'</div>'
		+	'</div>';

		if((index+1) % 2 == 0)
		{ 
			if(data.EngagementData.length != (index+1))
				EngagementData += '</div></div><div class="EngagementSlides" style="width:100%;text-align: center;"><div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">';
		}
	});

	EngagementData += '</div>'
		+	'</div>';

	$('#EngagementData').html(EngagementData);

	EngagementShowDivs(slideIndex);


	var ConsiderationData = '<div class="ConsiderationSlides" style="width:100%;text-align: center;">'
		+	'<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">'; 

	$.each(data.ConsiderationData, function( index, value ) {
 
		ConsiderationData += '<div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">'
		+		'<div class="alert-modal">'
		+			'<div class="modal">'
		+				'<div class="modal-dialog alertbox">'
		+				    '<div class="modal-content">'
		+				        '<div class="modal-header blue top_content_title">'         
		+				            '<h4 class="modal-title"><a href="/ContentDetail?PageName='+value.PageName+'&Date='+value.Date+'"  style="color: white">'+value.PageName+'</a></h4>'
		+				        '</div>'
		+				        '<div class="modal-body">'
		+				        	'<div class="row ">'
		+					            '<div class="col-md-12">'
		+					            	'<div class="alert-label">'
		+					            		'<div class="box-body">'
		+						            		'<div class="thumbnail_image">'       
		+						                   		'<a href="/ContentDetail?PageName='+value.PageName+'&Date='+value.Date+'">'
		+						                      		'<div id="screenshot" style="overflow: hidden; border: 1px solid black">'  
		+						                          		'<iframe src="'+value.PageURL+'" name= "tabsa" width="95%" height="100%" frameborder="0" allowtransparency="true" style="height: 250px;"></iframe>'  
		+						                       		'</div>'
		+						                   		'</a>'
		+						                	'</div>'
		+					                	'</div>'
		+								    '</div>'
		+								    '<div class="col-md-12 col-sm-12 col-xs-12">'
		+						            	 '<div class="col-lg-3" style="padding: 0px;">'
		+						            	 	'<h1  style="margin: 0px; color:#7CFC00"><strong>'+value.avgscore+'</+strong></h1>'
		+						            	 '</div>'								            			
		+						            	 '<div class="col-lg-9">'
		+						            	 	'<p>Prospects Generated: '+value.sum+' </p>'
		+						            	 	'<p>Posted: '+value.Date+'</p>'
		+						            	 '</div>'						            			
		+						            '</div>'
		+								'</div>'
		+							'</div>'		           
		+						'</div>'
		+					'</div>'
		+				'</div>'
		+			'</div>'
		+		'</div>'
		+	'</div>';

		if((index+1) % 2 == 0)
		{ 
			if(data.ConsiderationData.length != (index+1))
				ConsiderationData += '</div></div><div class="ConsiderationSlides" style="width:100%;text-align: center;"><div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">';
		}
	});

	ConsiderationData += '</div>'
		+	'</div>';

	$('#ConsiderationData').html(ConsiderationData);

	ConsiderationShowDivs(slideIndex);


	var ConversionData = '<div class="ConversionSlides" style="width:100%;text-align: center;">'
		+	'<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">'; 

	$.each(data.ConversionData, function( index, value ) {
 
		ConversionData += '<div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">'
		+		'<div class="alert-modal">'
		+			'<div class="modal">'
		+				'<div class="modal-dialog alertbox">'
		+				    '<div class="modal-content">'
		+				        '<div class="modal-header blue top_content_title">'         
		+				            '<h4 class="modal-title"><a href="/ContentDetail?PageName='+value.PageName+'&Date='+value.Date+'"  style="color: white">'+value.PageName+'</a></h4>'
		+				        '</div>'
		+				        '<div class="modal-body">'
		+				        	'<div class="row ">'
		+					            '<div class="col-md-12">'
		+					            	'<div class="alert-label">'
		+					            		'<div class="box-body">'
		+						            		'<div class="thumbnail_image">'       
		+						                   		'<a href="/ContentDetail?PageName='+value.PageName+'&Date='+value.Date+'">'
		+						                      		'<div id="screenshot" style="overflow: hidden; border: 1px solid black">'  
		+						                          		'<iframe src="'+value.PageURL+'" name= "tabsa" width="95%" height="100%" frameborder="0" allowtransparency="true" style="height: 250px;"></iframe>'  
		+						                       		'</div>'
		+						                   		'</a>'
		+						                	'</div>'
		+					                	'</div>'
		+								    '</div>'
		+								    '<div class="col-md-12 col-sm-12 col-xs-12">'
		+						            	 '<div class="col-lg-3" style="padding: 0px;">'
		+						            	 	'<h1  style="margin: 0px; color:#7CFC00"><strong>'+value.avgscore+'</+strong></h1>'
		+						            	 '</div>'								            			
		+						            	 '<div class="col-lg-9">'
		+						            	 	'<p>Prospects Generated: '+value.sum+' </p>'
		+						            	 	'<p>Posted: '+value.Date+'</p>'
		+						            	 '</div>'						            			
		+						            '</div>'
		+								'</div>'
		+							'</div>'		           
		+						'</div>'
		+					'</div>'
		+				'</div>'
		+			'</div>'
		+		'</div>'
		+	'</div>';

		if((index+1) % 2 == 0)
		{ 
			if(data.ConversionData.length != (index+1))
				ConversionData += '</div></div><div class="ConversionSlides" style="width:100%;text-align: center;"><div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">';
		}
	});

	ConversionData += '</div>'
		+	'</div>';

	$('#ConversionData').html(ConversionData);

	ConversionShowDivs(slideIndex);
	    
	});
}

function topKeywordsUsed(days=30)
{ 
	$.get("{{URL::to('/Content/topPerformingContent')}}",{days:days}).done(function( data ) {

		var string1 = '';
		var string2 = '';
		var string3 = '';
		var string4 = '';

		$.each(data.AwarenessData, function( index, value ) {
			string1 += value.PageName;
		});

		$.each(data.EngagementData, function( index, value ) {
			string2 += value.PageName;
		});

		$.each(data.ConsiderationData, function( index, value ) {
			string3 += value.PageName;
		});

		$.each(data.ConversionData, function( index, value ) {
			string4 += value.PageName;
		});
		
		if($("ul.nav-tabs li.tab01.active").length == 1)
		{
			$("#Word_chart1").empty();
	    	drawWordCloud(string1,"#Word_chart1");
		}

		if($("ul.nav-tabs li.tab02.active").length == 1)
		{
			$("#Word_chart2").empty();
	    	drawWordCloud(string2,"#Word_chart2");
		}

		if($("ul.nav-tabs li.tab03.active").length == 1)
		{
			$("#Word_chart3").empty();
	    	drawWordCloud(string3,"#Word_chart3");
		}

		if($("ul.nav-tabs li.tab04.active").length == 1)
		{
			$("#Word_chart4").empty();
	    	drawWordCloud(string4,"#Word_chart4");
		}
	    
    });
	
}

 /*function draw(words) 
 {		
 		  var element = "#Word_chart4";
 		  var width = $(element).width();
 		  var height = '400';
 		   var words = text_string.split(/[ '\-\(\)\*":;\[\]|{},.!?]+/);
          d3.select(element).append("svg")
              .attr("width", width)
              .attr("height", height)
            .append("g")
              .attr("transform", "translate(" + [width >> 1, height >> 1] + ")")
            .selectAll("text")
              .data(words)
            .enter().append("text")
              .style("font-size", function(d) { return xScale(d.value) + "px"; })
              .style("font-family", "Impact")
              .style("fill", function(d, i) { return fill(i); })
              .attr("text-anchor", "middle")
              .attr("transform", function(d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
              })
              .text(function(d) { return d.key; });
 }
 function drawWordCloud(text_string,element)
 {
        var common = text_string;
        var word_count = {};
        var words = text_string.split(/[ '\-\(\)\*":;\[\]|{},.!?]+/);
          if (words.length == 1){
            word_count[words[0]] = 1;
          } else {
            words.forEach(function(word){
              var word = word.toLowerCase();
              if (word != "" && common.indexOf(word)==-1 && word.length>1){
                if (word_count[word]){
                  word_count[word]++;
                } else {
                  word_count[word] = 1;
                }
              }
            })
          }
        var svg_location = element;
        var width = $(element).width();
        var height = '400';//$("#chart").height();
        var fill = d3.scale.category20();
        var word_entries = d3.entries(word_count);
        var xScale = d3.scale.linear()
           .domain([0, d3.max(word_entries, function(d) {
              return d.value;
            })
           ])
           .range([10,100]);
        d3.layout.cloud().size([width, height])
          .timeInterval(20)
          .words(word_entries)
          .fontSize(function(d) { return xScale(+d.value); })
          .text(function(d) { return d.key; })
          .rotate(function() { return ~~(Math.random() * 2) * 90; })
          .font("Impact")
          .on("end", draw)
          .start();
      
        d3.layout.cloud().stop();
}*/

 function drawWordCloud(text_string,element){
        var common = text_string;
        var word_count = {};
        var words = text_string.split(/[ '\-\(\)\*":;\[\]|{},.!?]+/);
          if (words.length == 1){
            word_count[words[0]] = 1;
          } else {
            words.forEach(function(word){
              var word = word.toLowerCase();
              if (word != "" && common.indexOf(word)==-1 && word.length>1){
                if (word_count[word]){
                  word_count[word]++;
                } else {
                  word_count[word] = 1;
                }
              }
            })
          }
        var svg_location = element;
        var width = $(element).width();
        var height = '400';//$("#chart").height();
        var fill = d3.scale.category20();
        var word_entries = d3.entries(word_count);
        var xScale = d3.scale.linear()
           .domain([0, d3.max(word_entries, function(d) {
              return d.value;
            })
           ])
           .range([10,100]);
        d3.layout.cloud().size([width, height])
          .timeInterval(20)
          .words(word_entries)
          .fontSize(function(d) { return xScale(+d.value); })
          .text(function(d) { return d.key; })
          .rotate(function() { return ~~(Math.random() * 2) * 90; })
          .font("Impact")
          .on("end", draw)
          .start();
        function draw(words) {
          d3.select(svg_location).append("svg")
              .attr("width", width)
              .attr("height", height)
            .append("g")
              .attr("transform", "translate(" + [width >> 1, height >> 1] + ")")
            .selectAll("text")
              .data(words)
            .enter().append("text")
              .style("font-size", function(d) { return xScale(d.value) + "px"; })
              .style("font-family", "Impact")
              .style("fill", function(d, i) { return fill(i); })
              .attr("text-anchor", "middle")
              .attr("transform", function(d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
              })
              .text(function(d) { return d.key; });
        }
        d3.layout.cloud().stop();
      }

 
 $(window).load(function(){

      $("#Word_chart1").empty();
 	  drawWordCloud(text_string1,"#Word_chart1");
});
</script>


@endsection



