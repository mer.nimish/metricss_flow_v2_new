@extends('template.template')

@section('content-header')
<link rel="stylesheet" href="{{URL::to('/css/LeadID.css')}}">
<link rel="stylesheet" href="{{URL::to('/css/dashboard.css')}}"> 
<section class="content-header">
	<div id="breadcrumbs">
		<h4 class="bold"><i class="fa fa-chevron-left" aria-hidden="true"></i><a href="/leads">All Leads</a>
			</h4>
	</div>
	<div id="client_logo">
		<img src="img/{{$client_id}}.png" alt="client_logo">
	</div>
</section>
<style type="text/css">
	.wd{
		word-wrap: break-word;
	}
	 .box-header{height: 48px; overflow: hidden;color: black; }
</style>
@endsection
@section('content')

<div class="main-content">
	<div class="row">
		<section id="UserID" >
				<div class="row" >
					<div class="col-md-12 col-lg-11 col-xs-12">
						<div class="col-md-12 col-lg-10 col-xs-12">
							<h3 class="bold">User ID: <span id="UD_UserID"></span></h3> 
						</div>
						<div class="col-md-12 col-lg-2 col-xs-12">
							<button type="button" class="float-right btn btn-primary btn-flat btn-sm">Push to CRM</button>
						</div>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12 col-lg-12 col-xs-12">
						<div class="col-md-11 col-xs-12 col-sm-12">
							<div class="col-lg-2">
								<center><img src="img/Lead.png" alt="client_logo" class="img-responsive"></center>
								<center><h4 id="Stage"></h4></center>
							</div>
							<div class="col-lg-3">
								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>Last Seen:</p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8 wd" >
										<p><strong id="UD_LastSeen"></strong></p>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>First Seen: </p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8 wd">
										<p><strong id="UD_FirstSeen"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>Location: </p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8 wd">
										<p><strong id="UD_Location"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>IP Address: </p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8 wd">
										<p><strong id="UD_IPAddress"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>Language: </p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8 wd">
										<p><strong id="UD_Language"></strong></p>
									</div>
								</div>

							</div>

							<div class="col-lg-4">
								<div class="row">
									<div class="col-lg-5 col-xs-12 col-sm-4">
										<p>Most Common Device: </p>
									</div>
									<div class="col-lg-7 col-xs-12 col-sm-8 wd">
										<p><strong id="UD_MostCommonDevice"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-5 col-xs-12 col-sm-4">
										<p>Device Brand: </p>
									</div>
									<div class="col-lg-7 col-xs-12 col-sm-8 wd">
										<p><strong id="UD_DeviceBrand"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-5 col-xs-12 col-sm-4">
										<p>Device Model: </p>
									</div>
									<div class="col-lg-7 col-xs-12 col-sm-8 wd">
										<p><strong id="UD_DeviceModel"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-5 col-xs-12 col-sm-4">
										<p>Tough Screen: </p>
									</div>
									<div class="col-lg-7 col-xs-12 col-sm-8 wd">
										<p><strong id="UD_ToughScreen"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-5 col-xs-12 col-sm-4">
										<p>Operating System: </p>
									</div>
									<div class="col-lg-7 col-xs-12 col-sm-8 wd">
										<p><strong id="UD_OperatingSystem"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-5 col-xs-12 col-sm-4">
										<p>Browser: </p>
									</div>
									<div class="col-lg-7 col-xs-12 col-sm-8 wd">
										<p><strong id="UD_Browser"></strong></p>
									</div>
								</div>
							</div>
							

							<div class="col-lg-3">
								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>Campaign: </p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8">
										<p><strong id="UD_Campaign"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>Channels: </p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8">
										<p><strong id="UD_Channels"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>Ad Blocker: </p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8">
										<p><strong id="UD_AdBlocker"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>Flash: </p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8">
										<p><strong id="UD_Flash"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>Html5: </p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8">
										<p><strong id="UD_Html5"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-xs-12 col-sm-4">
										<p>Siverlight: </p>
									</div>
									<div class="col-lg-8 col-xs-12 col-sm-8">
										<p><strong id="UD_Siverlight"></strong></p>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
		</section>
		<section id="Behavior" >
			<div class="row">
				<div class="col-md-12 col-lg-12 col-xs-12">
				<h3 class="bold">Behavior</h3>
				<div class="alert-modal">
					<div class="modal">
						<div class="col-md-11 col-xs-12 col-sm-12">
							<div class="modal-dialog">
							    <div class="modal-content">
							        <div class="modal-header light-blue">
							            <div class="col-lg-2">  
							            	<center><img src="img/daily03.png" alt="client_logo" class="img-responsive"></center>      
							            </div>
							            <div class="col-lg-5 col-xs-12 col-sm-12"> 
							            	<div class="row">
												<div class="col-lg-5 col-xs-12 col-sm-5">
													<p>Typical Active Time: </p>
												</div>
												<div class="col-lg-7 col-xs-12 col-sm-7 wd" >
													<p><strong id="BD_TypicalActiveTime"></strong></p>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-5 col-xs-12 col-sm-5">
													<p>Most Common Device: </p>
												</div>
												<div class="col-lg-7 col-xs-12 col-sm-7 wd" >
													<p><strong id="BD_MostCommonDevice"></strong></p>
												</div>
											</div>  
											<div class="row">
												<div class="col-lg-5 col-xs-12 col-sm-5">
													<p>Average Session Length: </p>
												</div>
												<div class="col-lg-7 col-xs-12 col-sm-7 wd">
													<p><strong id="BD_AverageSessionLength"></strong></p>
												</div>
											</div>  
											<div class="row">
												<div class="col-lg-5 col-xs-12 col-sm-5">
													<p>Avg.Clicks per Session: </p>
												</div>
												<div class="col-lg-7 col-xs-12 col-sm-7 wd">
													<p><strong id="BD_AvgClicksperSession"></strong></p>
												</div>
											</div>         

							            </div>
							        </div>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</section>

		<section id="DailyActivity">
			<div class="row">
				<div class="col-md-12 col-lg-12 col-xs-12">
				<h3 class="bold">Daily Activity</h3>
				<div class="alert-modal">
					<div class="modal">
						<div class="col-md-11 col-xs-12 col-sm-12">
							<div class="row">
						    	<div class="col-md-3">
						    		<center>
						    			<img src="img/daily01.png" alt="client_logo">
						    			<p></p>

						    			<p>Avg. Time: <strong id="DA_AvgTime1"></strong></p>
						    			<p>Avg. Clicks: <strong id="DA_AvgClicks1"></strong></p>
						    		</center>
						    	</div>
						    	<div class="col-md-3">
						    		<center>
						    			<img src="img/daily02.png" alt="client_logo">
						    			<p></p>

						    			<p>Avg. Time: <strong id="DA_AvgTime2"></strong></p>
						    			<p>Avg. Clicks: <strong id="DA_AvgClicks2"></strong></p>
						    		</center>
						    	</div>
						    	<div class="col-md-3">
						    		<center>
						    			<img src="img/daily03.png" alt="client_logo">
						    			<p></p>

						    			<p>Avg. Time: <strong id="DA_AvgTime3"></strong></p>
						    			<p>Avg. Clicks: <strong id="DA_AvgClicks3"></strong></p>
						    		</center>
						    	</div>
						    	<div class="col-md-3">
						    		<center>
						    			<img src="img/daily04.png" alt="client_logo">
						    			<p></p>

						    			<p>Avg. Time: <strong id="DA_AvgTime4"></strong></p>
						    			<p>Avg. Clicks: <strong id="DA_AvgClicks4"></strong></p>
						    		</center>
						    	</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</section>

		<section id="SessionProfile">
			<div class="row">
				<div class="col-md-12 col-lg-12 col-xs-12">
					<h3 class="bold">Session Profile</h3>
					<div class="alert-modal">
						<div class="modal">
							<div class="col-md-11 col-xs-12 col-sm-12">
								<div class="modal-dialog">
								    <div class="modal-content">
								        <div class="modal-header light-blue">
								            <div class="col-md-3">  
								            	
								            	<canvas id="donut-chart" height="150"></canvas>    
								            </div>
								            <div class="col-md-6"> 
								            	<p><strong><h3><span id="SP_TotalSessions"></span> Total Sessions</h3></strong></p>
								            	<br>
								            		
												    <div class="row" id="SessionDetailsDiv">
								                      	
								                    </div>
												

								            </div>

								            <div class="col-md-3">
								            	 <div class="row">
									            	 <div class="float-right">
									            	 	<strong style="font-size: 60px;" id="SP_PredictionAccuracy"></strong>
									            	 </div> 
									             </div>
								            	 <div class="row">
									            	<div class="float-right">
									            	 	<h5><strong>Prediction accuracy</strong></h5>
									            	</div>    
									             </div>
								            </div>
								            
								        </div>
								    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="History">
			<div class="row">
				<div class="col-md-11 col-lg-12 col-xs-12">

					<h3 class="bold">Lead Progression</h3>
					<div class="alert-modal">
						<div class="modal">
							<div class="col-md-11 col-xs-12 col-sm-12">
								<div class="modal-dialog">
								    <div class="modal-content">
								        <div class="modal-header light-blue">
								            <div class="col-md-3 ">
								            	<div class="row">
									                <a href="/content">
									                  	<div class="box-header with-border">
									                  		<h3 class="box-title" id="LP_PageTitle1"></h3>              
									                	</div>
									             	</a> 
									              	<div class="box-body">
									                	<div class="thumbnail_image">         
									                   		<a href="/content">
									                      		<div id="screenshot" style="overflow: hidden; border: 1px solid black">                           
									                          		<iframe id="LP_PageURL1" scrolling="no" src="" style="border:none;">    <!-- sandbox="allow-pointer-lock" -->                  
									                         		</iframe> 
									                       		</div>
									                   		</a>
									                	</div>
									                	<div class="col-md-12">
									                  		<h3><strong id="LP_PageDate1"></strong></h3>
									                	</div> 
									                	<div class="col-md-12">
									                  		<center><h3 id="LP_PageStage1"></h3></center> 			                  	
									                	</div>           
									              	</div>
									            </div>    
								            </div>

								            <div class="col-md-3"> 
								            	<div class="row">
									                <a href="/content">
									                  	<div class="box-header with-border">
									                  		<h3 class="box-title" id="LP_PageTitle2"> </h3>              
									                	</div>
									             	</a> 
									              	<div class="box-body">
									                	<div class="thumbnail_image">         
									                   		<a href="/content">
									                      		<div id="screenshot" style="overflow: hidden; border: 1px solid black">                           
									                          		<iframe id="LP_PageURL2" scrolling="no" src="" style="border:none;">
									                         		</iframe> 
									                       		</div>
									                   		</a>
									                	</div>
									                 
									                	<div class="col-md-12">
									                  		<h3><strong id="LP_PageDate2"></strong></h3>
									                	</div> 
									                	<div class="col-md-12">
									                    	<center><h3 id="LP_PageStage2"></h3> </center>
									                  									                  	
									                	</div>           
									              	</div>
									            </div>
									     </div>
								         <div class="col-md-3">
								            	<div class="row">
									                <a href="/content">
									                  	<div class="box-header with-border">
									                  		<h3 class="box-title" id="LP_PageTitle3"></h3>       
									                	</div>
									             	</a> 
									              	<div class="box-body">
									                	<div class="thumbnail_image">         
									                   		<a href="/content">
									                      		<div id="screenshot" style="overflow: hidden; border: 1px solid black">                           
									                          		<iframe id="LP_PageURL3" src="" name= "tabsa" width="95%" height="100%" frameborder="0" allowtransparency="true"></iframe>
									                       		</div>
									                   		</a>
									                	</div>
									                 
									                	<div class="col-md-12">
									                  		<h3><strong id="LP_PageDate3"></strong></h3>
									                	</div> 
									                	<div class="col-md-12">
									                    	<center>
									                  		<h3 id="LP_PageStage3"></h3></center>
									                  									                  	
									                	</div>           
									              	</div>
									            </div>
								            	
								            </div>
								            <div class="col-md-3">
								            	<div class="row">
									                <a href="/content">
									                  	<div class="box-header with-border">
									                  		<h3 class="box-title" id="LP_PageTitle4"></h3>         
									                	</div>
									             	</a> 
									              	<div class="box-body">
									                	<div class="thumbnail_image">         
									                   		<a href="/content">
									                      		<div id="screenshot" style="overflow: hidden; border: 1px solid black">                           
									                          		<iframe id="LP_PageURL4" scrolling="no" src="" style="border:none;">                      
									                         		</iframe> 
									                       		</div>
									                   		</a>
									                	</div>
									                 
									                	<div class="col-md-12">
									                  		<h3><strong id="LP_PageDate4"></strong></h3>
									                	</div> 
									                	<div class="col-md-12">
									                    	<center><h3 id="LP_PageStage4"> </h3> </center>    	
									                	</div>           
									              	</div>
									            </div>
								            </div>
								            
								        </div>
								    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="col-lg-11 connectedSortable ui-sortable">
				<br />
					<div class="col-md-12 col-lg-12 col-xs-12">
						<div class="col-md-10 col-sm-12 col-xs-12">
								<h3 class="bold">Top Paths to Conversion</h3>	
						</div>	
						<!-- <div class="col-md-2 col-sm-12 col-xs-12">
							<select class="form-control tableselect" onchange="topPathsConversion(this.value)">
								<option value="7">Last 7 Days</option>
								<option value="14">Last 14 Days</option>
								<option value="21">Last 21 Days</option>
								<option value="30">Last 30 Days</option>
							</select>
					    </div> -->
					</div>

					<div class="col-md-11 col-lg-12 col-xs-12">
						<div class="alert-modal">
							<div class="modal">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<div class="modal-dialog">
									    <div class="modal-content">
									        <div class="modal-header light-blue">
									        	<div class="pathChart_group">
									        	<div id="pathChart">
									        		<div class="pathpart">
									        			<div class="facebook"><center><i class="fa fa-facebook fa-3x" aria-hidden="true"></i></center></div>
									        		</div>
									        		
									        	</div>
										        	
									        	</div>
									        </div>
									    </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
</div>
</div>

<script src="{{URL::to('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script src="{{URL::to('plugins/flot/jquery.flot.min.js')}}"></script>
<script src="{{URL::to('plugins/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{URL::to('plugins/flot/jquery.flot.pie.min.js')}}"></script>


<!-- <script src="{{URL::to('js/LeadID.js')}}"></script> -->

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jcanvas/20.1.4/jcanvas.js"></script>
<script src="{{URL::to('js/dashboard_pathchart.js')}}"></script>

<script src="{{URL::to('plugins/chartjs/Chart.min.js')}}"></script>

<script type="text/javascript">

function topPathsConversion(days=100)
{ 
	$.get("{{URL::to('/dashboard/nodePath')}}",{"client_id":"{{$client_id}}"}).done(function( data ) {
		var pathdata=JSON.parse(data);
		var element="#pathChart";
		//$("#pathChart").empty();
		//$("#pathChart").append('<div class="facebook"><center><i class="fa fa-facebook fa-3x" aria-hidden="true"></i></center></div>');

		pathChart_plot(pathdata,element);
	});
}

function getLeadData(days = 30)
{ 
	$.get("{{URL::to('/Lead/getLeadData')}}",{"clientID":"{{Session::get('userdata')->client_id}}",days:days,e_id:"{{$e_id}}"}).done(function( data ) {

		$('#UD_UserID').html(data.userDetails.UserID);
		$('#UD_Stage').html(data.userDetails.Stage);
		$('#UD_LastSeen').html(data.userDetails.LastSeen);
		$('#UD_FirstSeen').html(data.userDetails.FirstSeen);
		$('#UD_Location').html(data.userDetails.Location);
		$('#UD_IPAddress').html(data.userDetails.IPAddress);
		$('#UD_Language').html(data.userDetails.Language);
		$('#UD_MostCommonDevice').html(data.userDetails.MostCommonDevice);
		$('#UD_DeviceBrand').html(data.userDetails.DeviceBrand);
		$('#UD_DeviceModel').html(data.userDetails.DeviceModel);
		$('#UD_ToughScreen').html(data.userDetails.ToughScreen);
		$('#UD_OperatingSystem').html(data.userDetails.OperatingSystem);
		$('#UD_Browser').html(data.userDetails.Browser);
		$('#UD_Campaign').html(data.userDetails.Campaign);
		$('#UD_Channels').html(data.userDetails.Channels);
		$('#UD_AdBlocker').html(data.userDetails.AdBlocker);
		$('#UD_Flash').html(data.userDetails.Flash);
		$('#UD_Html5').html(data.userDetails.Html5);
		$('#UD_Siverlight').html(data.userDetails.Siverlight);

		$('#BD_TypicalActiveTime').html(data.behaviorData.TypicalActiveTime);
		$('#BD_MostCommonDevice').html(data.behaviorData.MostCommonDevice);
		$('#BD_AverageSessionLength').html(data.behaviorData.AverageSessionLength);
		$('#BD_AvgClicksperSession').html(data.behaviorData.AvgClicksperSession);

		$('#DA_AvgTime1').html(data.dailyActivity.AvgTime1);
		$('#DA_AvgClicks1').html(data.dailyActivity.AvgClicks1);
		$('#DA_AvgTime2').html(data.dailyActivity.AvgTime2);
		$('#DA_AvgClicks2').html(data.dailyActivity.AvgClicks2);
		$('#DA_AvgTime3').html(data.dailyActivity.AvgTime3);
		$('#DA_AvgClicks3').html(data.dailyActivity.AvgClicks3);
		$('#DA_AvgTime3').html(data.dailyActivity.AvgTime3);
		$('#DA_AvgClicks3').html(data.dailyActivity.AvgClicks3);
		$('#DA_AvgTime4').html(data.dailyActivity.AvgTime4);
		$('#DA_AvgClicks4').html(data.dailyActivity.AvgClicks4);

		var SessionDetailsDiv = '';

		$.each(data.sessionProfile.SessionDetails, function( index, value ) {
			SessionDetailsDiv += '<div class="float col-xs-1">' +
	        	'<i class="fa fa-stop fa-2x labeltext" style="color:'+value.color+'; margin-top: 5px; "></i>' + 
	      	'</div>' +
	      	'<div class="labeltext col-xs-11">' +
	        	'<h4>'+value.value+' '+value.label+'</h4>' +
	      	'</div>';
	     });

		 $('#SessionDetailsDiv').html(SessionDetailsDiv);

		  var pieChartCanvas = $("#donut-chart").get(0).getContext("2d");
		  var pieChart = new Chart(pieChartCanvas);

		  var PieData = data.sessionProfile.SessionDetails;
		 
		  var pieOptions = {
		    segmentShowStroke: true,
		    segmentStrokeColor: "#fff",
		    segmentStrokeWidth: 1,
		    percentageInnerCutout: 50, // This is 0 for Pie charts
		    animationSteps: 100,
		    animationEasing: "easeOutBounce",
		    animateRotate: true,
		    animateScale: false,
		    responsive: true,
		    maintainAspectRatio: false,
		    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
		    tooltipTemplate: "<%=value %> <%=label%> users"
		  };
		  pieChart.Doughnut(PieData, pieOptions);
      	

      	$('#SP_TotalSessions').html(data.sessionProfile.TotalSessions);
		$('#SP_PredictionAccuracy').html(data.sessionProfile.PredictionAccuracy+'%');

      	$('#LP_PageTitle1').html(data.leadProgression.PageTitle1);
		$('#LP_PageDate1').html(data.leadProgression.PageDate1);
		$('#LP_PageStage1').html(data.leadProgression.PageStage1);
		$('#LP_PageURL1').attr("src", data.leadProgression.PageURL1);

		$('#LP_PageTitle2').html(data.leadProgression.PageTitle2);
		$('#LP_PageDate2').html(data.leadProgression.PageDate2);
		$('#LP_PageStage2').html(data.leadProgression.PageStage2);
		$('#LP_PageURL2').attr("src", data.leadProgression.PageURL2);

		$('#LP_PageTitle3').html(data.leadProgression.PageTitle3);
		$('#LP_PageDate3').html(data.leadProgression.PageDate3);
		$('#LP_PageStage3').html(data.leadProgression.PageStage3);
		$('#LP_PageURL3').attr("src", data.leadProgression.PageURL3);

      	$('#LP_PageTitle4').html(data.leadProgression.PageTitle4);
		$('#LP_PageDate4').html(data.leadProgression.PageDate4);
		$('#LP_PageStage4').html(data.leadProgression.PageStage4);
		$('#LP_PageURL4').attr("src", data.leadProgression.PageURL4);
	
	});
}

$( document ).ready(function() {

  getLeadData();

  topPathsConversion();

});


 
</script>

@endsection



