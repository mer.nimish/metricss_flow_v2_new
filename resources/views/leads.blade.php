@extends('template.template')

@section('content-header') 
<link rel="stylesheet" href="{{URL::to('/css/leads.css')}}">
	<section class="content-header">
		<div id="breadcrumbs">
			<h4 class="bold"><i class="fa fa-chevron-left" aria-hidden="true"></i>All Accounts</h4>
		</div>
		<div id="client_logo">
			<img src="img/{{$client_id}}.png" alt="client_logo">
		</div>
	</section>
@endsection
@section('content')
<div class="main-content">
<div class="row">
<style type="text/css">
   .bar_avg_visitor{
      height: 40px;
       
      padding: 0;
      margin-top: 10px;margin-bottom: 10px;
   }
   .align{
   	display: flex;
   	flex-direction: column;
   }
</style>

 <section class="content">
      <div class="row">
        <div class="col-lg-11">
          <div class="">
            <div class="box-header">
              <h3 class="box-title">Recent Leads</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="bt_dt" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>User ID</th>
                  <th>Last Seen</th>
                  <th>Channel</th>
                  <th>Latest Content</th>
                  <th>Cookie</th>
                  <th>Stage</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($recentLeads['LeadList'] as $value)
	                <tr>
	                  <td><a href="/LeadID?e_id={{$value['UserID']}}">{{$value['UserID']}}</a></td>
	                  <td>{{$value['LastSeen']}}</td>
	                  <td>{{$value['Channel']}}</td>
	                  <td><a href="/ContentDetail?PageName={{$value['LatestContent']}}&Date={{$value['Date']}}">{{$value['LatestContent']}}</a></td>
	                  <td>{{$value['Cookie']}}</td>
	                  <td>{{$value['Stage']}}</td>
	                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

<section id="leadfunnel">
	<div class="row">
		<div class="col-md-11 col-sm-12 col-xs-12">
		<div class="col-md-10 col-sm-12 col-xs-12">
			<h3 class="bold">Lead Funnel</h3>
		</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<select class="form-control tableselect" style="border:none;" onchange="leadsFunnel(this.value);">
					<option value="7">Last 7 Days</option>
					<option value="14">Last 14 Days</option>
					<option value="21">Last 21 Days</option>
					<option value="30" selected>Last 30 Days</option>
				</select>
			</div>
		</div>

		<div id='leadfunnel_loader' class="col-md-11 col-sm-12 col-xs-12" style='display: none;text-align:center;'><img src='{{URL::to('/img/loader.gif')}}'></div>
		<div id="leadfunnel_contain" class="col-md-11 col-sm-12 col-xs-12">
			<!-- <div class="col-lg-8 col-md-12">
				<div class="box box-primary" style="border-top: none;">
					<div class="box-body light-blue">
						<div class="col-md-12">
							<div id="graphic"></div>
						</div>
						 
					</div>
	            </div>
            </div> -->
            <div class="col-md-8 col-sm-12 col-xs-12">
	            <div class="col-lg-12 col-md-12" style="border: 1px solid #EEEEEE;border-radius: 12px;box-shadow: 0 2px 3px rgba(0,0,0,0.125);">
	            	<div class="col-md-6 col-sm-12 col-xs-12">
	            		<div id="lead_funnel_barchart1" class="modal-body">
		        		</div>
	            	</div>
	            	<div class="col-md-6 col-sm-12 col-xs-12 align">
		        		<div id="lead_funnel_barchart1_data" class="modal-body">
		        		</div>
	        		</div>
	        	</div>
	        
	        	<div class="col-lg-12 col-md-12" style="border: 1px solid #EEEEEE;border-radius: 12px;box-shadow: 0 2px 3px rgba(0,0,0,0.125);">
	        		<div class="col-md-6 col-sm-12 col-xs-12">
	        			<div class="modal-body" id="lead_funnel_barchart2">
		                    
		                </div>
	        		</div>
	        		<div class="col-md-6 col-sm-12 col-xs-12 align">
	        			<div class="modal-body" id="lead_funnel_barchart2_data" >
	        			
	        			</div>
	        		</div>
	        	</div>
        	</div>
            <div class="col-lg-4 col-md-12 col-xs-12">
            	<div class="box box-primary" style="border-top: none;">
					<div class="box-header light-blue">
						<h3><strong>Cookie Usage</strong></h3>
					</div>
					<div class="box-body">
                    <!-- <div id="donut-chart"></div> -->
	                    
	                    <div class="col-md-12">
		                    <div class="chart-responsive" >
		                        <canvas id="pieChart" height="150"></canvas>
		                    </div>
	                        <ul class="chart-legend clearfix">
	                            <li><i class="fa fa-circle-o text-light-blue"></i> Leads that allow cookies</li>
	                            <li><i class="fa fa-circle-o text-red"></i> Leads that block cookies</li>
	                        </ul>
	                    </div>
                  </div>
	            </div>
            </div>
		</div>
	</div>

	 
</section>
<section id="breakdown">
	
	<div class="row">
		<div class="col-md-11 col-sm-12 col-xs-12">
			<div class="col-md-10 col-sm-12 col-xs-12">
				<h3 class="bold">Lead Breakdown</h3>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<select class="form-control tableselect" style="border:none;" onchange="leadsBreakdown(this.value);">
					<option value="7">Last 7 Days</option>
					<option value="14">Last 14 Days</option>
					<option value="21">Last 21 Days</option>
					<option value="30" selected>Last 30 Days</option>
				</select>
			</div>
		</div>

		<div id='breakdown_loader' class="col-md-11 col-sm-12 col-xs-12" style='display: none;text-align:center;'><img src='{{URL::to('/img/loader.gif')}}'></div>
		<div id="breakdown_contain" class="col-md-11 col-sm-12 col-xs-12">
			<div class="col-lg-8 col-md-12">
				<div class="box box-primary" style="border-top: none;">
					<div class="box-header light-blue">
						<h4><strong>Total Visits & Unique Leads</strong></h4>
					</div>
					<div id="bar-chart"></div>
					<br />
					<div id="labels">
	                    <div class="col-md-4">
	                     	<ul class="chart-legend clearfix">
	                            <li><i class="fa fa-circle-o text-light-blue"></i> Total Visits</li>
	                        </ul>
	                    </div>
	                    <div class="col-md-4">
	                     	<ul class="chart-legend clearfix">
	                            <li><i class="fa fa-circle-o text-green"></i> Unique Leads</li>
	                        </ul>
	                    </div>
	                </div>
	            </div>
            </div>
            <div class="col-lg-4 col-md-12">
            	<div class="box box-primary" style="border-top: none;">
					<div class="box-header light-blue">
						<h4><strong>OS Usage</strong></h4>
					</div>
					<div class="box-body">
                    <!-- <div id="donut-chart"></div> -->
	                    
	                    <div class="col-md-12">
		                    <div class="chart-responsive">
		                        <canvas id="pieChart2" height="150"></canvas>
		                    </div>
	                        <ul class="chart-legend clearfix">
	                            <li><i class="fa fa-circle-o text-light-blue"></i> Windows OS</li>
	                            <li><i class="fa fa-circle-o text-red"></i> Mac OSX</li>
	                            <li><i class="fa fa-circle-o text-green"></i> iOS</li>
	                            <li><i class="fa fa-circle-o text-yellow"></i> Anroid OS</li>
	                        </ul>
	                    </div>
                  </div>
	            </div>
            </div>
		</div>
	</div>
</section>
<section id="channels">
	<div class="row">
		<div class="col-md-11 col-sm-12 col-xs-12">
			<div class="col-md-10 col-sm-12 col-xs-12">
				<h3 class="bold">Channels Driving Conversion</h3>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<select class="form-control tableselect" style="border:none;" onchange="channelsDrivingConversion(this.value);">
					<option value="7">Last 7 Days</option>
					<option value="14">Last 14 Days</option>
					<option value="21">Last 21 Days</option>
					<option value="30" selected>Last 30 Days</option>
				</select>
			</div>
		</div>

		<div id='channelsDrivingConversion_loader' class="col-md-11 col-sm-12 col-xs-12" style='display: none;text-align:center;'><img src='{{URL::to('/img/loader.gif')}}'></div>
		<div id="channelsDrivingConversion_contain" class="col-md-11 col-sm-12 col-xs-12">
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="alert-modal">
					<div class="modal">
						<div class="modal-dialog alertbox">
						    <div class="modal-content">
						        <div class="modal-header blue">         
						            <h4 class="modal-title text-align" id="ChannelName1"></h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row left">
							            <div class="col-md-12 alert-content">
							            	<div class="alert-label">
							            		<h4 class="bold600" id="ChannelText1"></h4>
								            	<div class="text-align box-data">
								            	 
								            		<div class="display">
								            			<h1> <span id="ChannelPer1"></span></h1>
								            		</div>							 
								            	</div>
										    </div>
										    <div class="col-md-12 col-sm-6 col-xs-12">
								            	<div>
								            		<h5>Conversion Rate: <span id="ConversionRate1"></span></h5>
								            	</div>
								            	<div>
								            		<h5>Prospects Generated: <span id="ProspectsGenerated1"></span></h5>
								            	</div>
								            	<div>
								            		<h5>Lead Conversions: <span id="LeadConversions1"></span></h5>
								            	</div>								            			
								            </div>
										</div>
									</div>			           
								</div>
							</div>
						<!-- /.modal-content -->
						</div>
					</div>
					<!-- /.modal-dialog -->
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="alert-modal">
					<div class="modal">
						<div class="modal-dialog alertbox">
						    <div class="modal-content">
						        <div class="modal-header blue">         
						            <h4 class="modal-title text-align" id="ChannelName2"></h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row left">
							            <div class="col-md-12 alert-content">
							            	<div class="alert-label">
							            		<h4 class="bold600" id="ChannelText2"></h4>
								            	<div class="text-align box-data">
								            		<div class="display">
								            			<h1> <span id="ChannelPer2"></span></h1>
								            		</div>
								            								 
								            	</div>
										    </div>
										    <div class="col-md-12 col-sm-6 col-xs-12">
								            			<div>
								            				<h5>Conversion Rate: <span id="ConversionRate2"></span></h5>
								            			</div>
								            			<div>
								            				<h5>Prospects Generated: <span id="ProspectsGenerated2"></span></h5>
								            			</div>
								            			<div>
								            				<h5>Lead Conversions: <span id="LeadConversions2"></span></h5>
								            			</div>								            			
								            		</div>
										</div>
									</div>			           
								</div>
							</div>
						<!-- /.modal-content -->
						</div>
					</div>
					<!-- /.modal-dialog -->
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="alert-modal">
					<div class="modal">
						<div class="modal-dialog alertbox">
						    <div class="modal-content">
						        <div class="modal-header blue">         
						            <h4 class="modal-title text-align" id="ChannelName3"></h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row left">
							            <div class="col-md-12 alert-content">
							            	<div class="alert-label">
							            		<h4 class="bold600" id="ChannelText3"></h4>
								            	<div class="text-align box-data">
								            		<div class="display">
								            			<h1> <span id="ChannelPer3"></span></h1>
								            		</div>					 
								            	</div>
										    </div>
										    <div class="col-md-12 col-sm-6 col-xs-12">
								            			<div>
								            				<h5>Conversion Rate: <span id="ConversionRate3"></span></h5>
								            			</div>
								            			<div>
								            				<h5>Prospects Generated: <span id="ProspectsGenerated3"></span></h5>
								            			</div>
								            			<div>
								            				<h5>Lead Conversions: <span id="LeadConversions3"></span></h5>
								            			</div>								            			
								            		</div>
										</div>
									</div>			           
								</div>
							</div>
						<!-- /.modal-content -->
						</div>
					</div>
					<!-- /.modal-dialog -->
				</div>
			</div>
		</div>
	</div>
</section>
<section id="conversions">
	<div class="row">
		<div class="col-md-11 col-sm-12 col-xs-12">
			<div class="col-md-10 col-sm-12 col-xs-12">
				<h3 class="bold">Content Driving Conversion</h3>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<select class="form-control tableselect" style="border:none;" onchange="contentDrivingConversion(this.value);">
					<option value="7">Last 7 Days</option>
					<option value="14">Last 14 Days</option>
					<option value="21">Last 21 Days</option>
					<option value="30" selected>Last 30 Days</option>
				</select>
			</div>
		</div>

		<div id='contentDriving1Div_loader' class="col-md-11 col-sm-12 col-xs-12" style='display: none;text-align:center;'><img src='{{URL::to('/img/loader.gif')}}'></div>
		<div id="contentDriving1Div_contain"  class="col-md-11 col-sm-12 col-xs-12">
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="alert-modal">
					<div class="modal">
						<div class="modal-dialog alertbox">
						    <div class="modal-content">
						        <div class="modal-header blue">         
						            <h4 class="modal-title text-align" id="ContentTitle1"></h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row left">
							            <div class="col-md-12 alert-content m10">
							            	<div class="alert-label">
								            	<div class="text-align box-data">
								            		<div class="thumbnail_image">         
									                   	<a href="/content">
									                    	<div class="ContentUrl1"  id="screenshot" style="overflow: hidden; border: 1px solid black">                        
									                       	<iframe sandbox="allow-pointer-lock" scrolling="no" src='https://verafin.com/' style="border:none;;height: 200px">                 
									                        </iframe> 
									                       	</div>
									                   	</a>
									                </div>
								            	</div>
								            </div>
								            <div class="col-md-2" style="margin-left: -20px;">
												<h1 class="green-trend" id="ContentPer1"></h1>
											</div>
											<div class="col-md-10" style="margin-left: 20px; padding-top: 15px;">
												<p style="font-size: 12px;">Prospects Generated: <span id="content_ProspectsGenerated1"></span></p>
												<p style="font-size: 12px;">Posted: <strong id="Posted1"></strong></p>
											</div>	
										</div>
									</div>	           
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
					</div>
					<!-- /.modal-dialog -->
				</div>
			</div>
			<div id="contentDriving2Div" class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="alert-modal">
					<div class="modal">
						<div class="modal-dialog alertbox">
						    <div class="modal-content">
						        <div class="modal-header blue">         
						            <h4 class="modal-title text-align" id="ContentTitle2"></h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row left">
							            <div class="col-md-12 m10 alert-content">
							            	<div class="alert-label">							            		
							            		<div class="text-align box-data">
							            			<div class="thumbnail_image">         
									                   	<a href="/content">
									                    	<div class="ContentUrl2" id="screenshot" style="overflow: hidden; border: 1px solid black">                        
									                       	<iframe sandbox="allow-pointer-lock" scrolling="no" src='https://verafin.com/' style="border:none;;height: 200px">           
									                        </iframe> 
									                       	</div>
									                   	</a>
									                </div>
									                <div class="col-md-12">
									                	<div class="col-md-2">
									                		<h2></h2>
									                	</div>
									                	<div class="col-md-8"></div>
									                </div>
							            		</div>
							            	</div>
							            	<div class="col-md-2" style="margin-left: -20px; margin-top: -20px;">
												<h1 class="green-trend" id="ContentPer2"></h1>
											</div>
											<div class="col-md-10" style="margin-left: 20px;">
												<p style="font-size: 12px;">Prospects Generated: <span id="content_ProspectsGenerated2"></span></p>
												<p style="font-size: 12px;">Posted: <strong id="Posted2"></strong></p>
											</div>	
							            </div>
							        </div>			           
						        </div>
						    </div>
						    <!-- /.modal-content -->
						</div>
					</div>
				    <!-- /.modal-dialog -->
				</div>
			</div>
			<div id="contentDriving3Div" class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="alert-modal">
					<div class="modal">
						<div class="modal-dialog alertbox">
						    <div class="modal-content">
						        <div class="modal-header blue">         
						            <h4 class="modal-title text-align" id="ContentTitle3"></h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row left">
							            <div class="col-md-12 alert-content m10">
							            	<div class="alert-label">							        
							            		<div class="text-align box-data">
							            			<div class="thumbnail_image">         
									                   	<a href="/content">
									                    	<div class="ContentUrl3" id="screenshot" style="overflow: hidden; border: 1px solid black">                        
									                       	<iframe sandbox="allow-pointer-lock" scrolling="no" src='https://verafin.com/' style="border:none;height: 200px">                 
									                        </iframe> 
									                       	</div>
									                   	</a>
									                </div>
										        </div>
										    </div>
										    <div class="col-md-2" style="margin-left: -20px;">
												<h1 style="color: #2E6FAB;" id="ContentPer3"></h1>
											</div>
											<div class="col-md-10" style="margin-left: 20px; padding-top: 15px;">
												<p style="font-size: 12px;">Prospects Generated: <span id="content_ProspectsGenerated3"></span></p>
												<p style="font-size: 12px;">Posted: <strong id="Posted3"></strong></p>
											</div>	
										</div>
									</div>			           
							    </div>
							</div>
							<!-- /.modal-content -->
						</div>
					</div>
					<!-- /.modal-dialog -->
				</div>
			</div>
		</div>
	</div>
</section>
<script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{URL::to('plugins/morris/morris.min.js')}}"></script>
<!-- <script type="text/javascript" src="{{URL::to('js/barchart.js')}}"></script> -->
<!-- <script type="text/javascript" src="{{URL::to('js/donut.js')}}"></script> -->
<script src="https://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<script type="text/javascript" src="{{URL::to('js/horizontal-chart.js')}}"></script>

<script src="{{URL::to('plugins/chartjs/Chart.min.js')}}"></script>

<script>
  $(function () {
    $('#bt_dt').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });

function leadsFunnel(days = 30)
{ 
	
	$('#leadfunnel_contain').hide();
	$('#leadfunnel_loader').show();
	$.get("{{URL::to('/Lead/leadsFunnel')}}",{"clientID":"{{Session::get('userdata')->client_id}}",days:days}).done(function( data ) {

			$('#leadfunnel_contain').show();
			$('#leadfunnel_loader').hide();
			var lead_funnel_barchart1 = ''; 
			var lead_funnel_barchart1_data = '';
			$.each(data.barChart1, function( index, value ) {

			 	lead_funnel_barchart1 += '<div class="col-md-12 col-sm-12 col-xs-12  bar_avg_visitor">' 
		        +      '<div class="float-right" style="width:'+value.SessionPer+'%;height:100%;background-color: '+value.SessionColor+';"></div>'
		        + '</div>';

	        	lead_funnel_barchart1_data += '<div class="col-md-12 col-sm-12 col-xs-12  bar_avg_visitor">'
	        	+	'<div class="float-right" style="width:100%;height:100%;padding-top:8px;"><b>'+value.TotalSession+' '+value.SessionTitle+'</b></div>'
	        	+'</div>';
	        	
			});
			$('#lead_funnel_barchart1').html(lead_funnel_barchart1);
			$('#lead_funnel_barchart1_data').html(lead_funnel_barchart1_data);


			var lead_funnel_barchart2 = ''; 
			var lead_funnel_barchart2_data = '';
			$.each(data.barChart2, function( index, value ) {

			 	lead_funnel_barchart2 += '<div class="col-md-12 col-sm-12 col-xs-12  bar_avg_visitor">' 
		        +      '<div class="float-right" style="width:'+value.SessionPer+'%;height:100%;background-color: '+value.SessionColor+';"></div>'
		        + '</div>';

	        	lead_funnel_barchart2_data += '<div class="col-md-12 col-sm-12 col-xs-12  bar_avg_visitor">'
	        	+	'<div class="float-right" style="width:100%;height:100%;padding-top:8px;"><b>'+value.TotalSession+' '+value.SessionTitle+'</b></div>'
	        	+'</div>';
	        	
			});
			$('#lead_funnel_barchart2').html(lead_funnel_barchart2);
			$('#lead_funnel_barchart2_data').html(lead_funnel_barchart2_data);


			  // Get context with jQuery - using jQuery's .get() method.
			 
			  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");

			  var pieChart = new Chart(pieChartCanvas);

			  var PieData = data.cookieUsage;
			  /*var PieData = [
			    {
			      value: 800,
			      color: "#3c8dbc",
			      highlight: "#3c8dbc",
			      label: "Leads that allow cookies"
			    },
			    {
			      value: 200,
			      color: "red",
			      highlight: "red",
			      label: "Leads that block cookies"
			    },
			  ];*/
			  var pieOptions = {
			    //Boolean - Whether we should show a stroke on each segment
			    segmentShowStroke: true,
			    //String - The colour of each segment stroke
			    segmentStrokeColor: "#fff",
			    //Number - The width of each segment stroke
			    segmentStrokeWidth: 1,
			    //Number - The percentage of the chart that we cut out of the middle
			    percentageInnerCutout: 50, // This is 0 for Pie charts
			    //Number - Amount of animation steps
			    animationSteps: 100,
			    //String - Animation easing effect
			    animationEasing: "easeOutBounce",
			    //Boolean - Whether we animate the rotation of the Doughnut
			    animateRotate: true,
			    //Boolean - Whether we animate scaling the Doughnut from the centre
			    animateScale: false,
			    //Boolean - whether to make the chart responsive to window resizing
			    responsive: true,
			    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
			    maintainAspectRatio: false,
			    //String - A legend template
			    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
			    //String - A tooltip template
			    tooltipTemplate: "<%=value %> <%=label%> users"
			  };
			  //Create pie or douhnut chart
			  // You can switch between pie and douhnut using the method below.
			  pieChart.Doughnut(PieData, pieOptions);
			  
	});
}


function leadsBreakdown(days = 30)
{ 
	$('#breakdown_contain').hide();
	$('#breakdown_loader').show();
	$.get("{{URL::to('/Lead/leadsBreakdown')}}",{"clientID":"{{Session::get('userdata')->client_id}}",days:days}).done(function( data ) {

				$('#breakdown_contain').show();
				$('#breakdown_loader').hide();

				$('#bar-chart').empty();
				var bar = new Morris.Bar({
			      element: 'bar-chart',   
			      data: data.totalVisitsUniqueLeads
			      /*[{y: ['M-15'], a: 100, b: 10},{y: ['16'], a: 200, b: 80} ]*/,      
			      barColors: ['#32CD32','#3c8dbc'],
			      xkey: 'y',
			      ykeys: ['a', 'b'],
			      /*labels: ['Unique Leads', 'Total Visitors'],*/
			      hideHover: 'auto',
			      stacked: true,
			      axes: true,
			      grid: false,
			      barSize: 10,
			      barGap: 0.05,
			      barSizeRatio: 1,
			      resize: true,
			      width: true,
			      xLabelMargin: 0
			    });

			  // Get context with jQuery - using jQuery's .get() method.
			
			  var pieChartCanvas = $("#pieChart2").get(0).getContext("2d");
			 
			  var pieChart = new Chart(pieChartCanvas);

			  var PieData = data.osUsage;
			 
			  var pieOptions = {
			    segmentShowStroke: true,
			    segmentStrokeColor: "#fff",
			    segmentStrokeWidth: 1,
			    percentageInnerCutout: 50, // This is 0 for Pie charts
			    animationSteps: 100,
			    animationEasing: "easeOutBounce",
			    animateRotate: true,
			    animateScale: false,
			    responsive: true,
			    maintainAspectRatio: false,
			    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
			    tooltipTemplate: "<%=value %> <%=label%> users"
			  };
			  pieChart.Doughnut(PieData, pieOptions);
			  
	});
}

function channelsDrivingConversion(days = 30)
{ 
	$('#channelsDrivingConversion_contain').hide();
	$('#channelsDrivingConversion_loader').show();
	$.get("{{URL::to('/Lead/channelsDrivingConversion')}}",{"clientID":"{{Session::get('userdata')->client_id}}",days:days}).done(function( data ) {

		$('#channelsDrivingConversion_contain').show();
		$('#channelsDrivingConversion_loader').hide();

		var flag = data.channelData1.IsChannelUp?'<i class="fa fa-arrow-up green-trend" aria-hidden="true"></i>':'<i class="fa fa-arrow-down red-trend" aria-hidden="true"></i>';
		$('#ChannelName1').html(data.channelData1.ChannelName);
		$('#ChannelText1').html(data.channelData1.ChannelText);
		$('#ChannelPer1').html(flag+' '+data.channelData1.ChannelPer);
		$('#ConversionRate1').html(data.channelData1.ConversionRate);
		$('#ProspectsGenerated1').html(data.channelData1.ProspectsGenerated);
		$('#LeadConversions1').html(data.channelData1.LeadConversions);

		var flag2 = data.channelData2.IsChannelUp?'<i class="fa fa-arrow-up green-trend" aria-hidden="true"></i>':'<i class="fa fa-arrow-down red-trend" aria-hidden="true"></i>';
		$('#ChannelName2').html(data.channelData2.ChannelName);
		$('#ChannelText2').html(data.channelData2.ChannelText);
		$('#ChannelPer2').html(flag2+' '+data.channelData2.ChannelPer);
		$('#ConversionRate2').html(data.channelData2.ConversionRate);
		$('#ProspectsGenerated2').html(data.channelData2.ProspectsGenerated);
		$('#LeadConversions2').html(data.channelData2.LeadConversions);

		var flag3 = data.channelData3.IsChannelUp?'<i class="fa fa-arrow-up green-trend" aria-hidden="true"></i>':'<i class="fa fa-arrow-down red-trend" aria-hidden="true"></i>';
		$('#ChannelName3').html(data.channelData3.ChannelName);
		$('#ChannelText3').html(data.channelData3.ChannelText);
		$('#ChannelPer3').html(flag3+' '+data.channelData3.ChannelPer);
		$('#ConversionRate3').html(data.channelData3.ConversionRate);
		$('#ProspectsGenerated3').html(data.channelData3.ProspectsGenerated);
		$('#LeadConversions3').html(data.channelData3.LeadConversions);
	});
}
function contentDrivingConversion(days = 30)
{ 
	
	$('#contentDriving1Div_contain').hide();
	$('#contentDriving1Div_loader').show();
	$.get("{{URL::to('/Lead/contentDrivingConversion')}}",{"clientID":"{{Session::get('userdata')->client_id}}",days:days}).done(function( data ) {
 
 		$('#contentDriving1Div_contain').show();
		$('#contentDriving1Div_loader').hide();

		if(data.contentData1 == undefined)
			$('#contentDriving1Div').hide();
		else {
			$('#contentDriving1Div').show();
			$('#ContentTitle1').html(data.contentData1.ContentTitle);
			$('.ContentUrl1').html('<iframe scrolling="no" src="'+data.contentData1.ContentUrl+'" style="border:none;height: 200px"></iframe>');
			$('#ContentPer1').html(data.contentData1.ContentPer);
			$('#content_ProspectsGenerated1').html(data.contentData1.ProspectsGenerated);
			$('#Posted1').html(data.contentData1.Posted);
		}

		if(data.contentData2 == undefined)
			$('#contentDriving2Div').hide();
		else {
			$('#contentDriving2Div').show();
			$('#ContentTitle2').html(data.contentData2.ContentTitle);
			$('.ContentUrl2').html('<iframe scrolling="no" src="'+data.contentData2.ContentUrl+'" style="border:none;height: 200px"></iframe>');
			$('#ContentPer2').html(data.contentData2.ContentPer);
			$('#content_ProspectsGenerated2').html(data.contentData2.ProspectsGenerated);
			$('#Posted2').html(data.contentData2.Posted);
		}

		if(data.contentData3 == undefined)
			$('#contentDriving3Div').hide();
		else {
			$('#contentDriving3Div').show();
			$('#ContentTitle3').html(data.contentData3.ContentTitle);
			$('.ContentUrl3').html('<iframe scrolling="no" src="'+data.contentData3.ContentUrl+'" style="border:none;height: 200px"></iframe>');
			$('#ContentPer3').html(data.contentData3.ContentPer);
			$('#content_ProspectsGenerated3').html(data.contentData3.ProspectsGenerated);
			$('#Posted3').html(data.contentData3.Posted);
		}
	});
}
//$( document ).ready(function() {

  leadsFunnel();
  channelsDrivingConversion();
  contentDrivingConversion();
  leadsBreakdown();
//});

 </script>
@endsection