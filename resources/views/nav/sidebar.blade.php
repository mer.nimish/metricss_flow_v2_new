<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <!-- <ul class="sidebar-menu" style="text-align:center;">
      
        <li class="treeview">
          <a href="{{URL::to('dashboard')}}">
            <i class="fa fa-dashboard" style="font-size: 4em;width:100%;"></i> <div>Dashboard</div>
          </a>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-dot-circle-o" style="font-size: 6em;width:100%;"></i> <div>Leads</div>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{URL::to('LeadID')}}"><i class="fa fa-circle-o"></i>Lead</a></li>
            <li><a href="{{URL::to('leads')}}"><i class="fa fa-circle-o"></i>Lead List</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt" style="font-size: 4em;width:100%;"></i> <div>Content</div>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{URL::to('Content')}}"><i class="fa fa-circle-o"></i>Content List</a></li>
            <li><a href="{{URL::to('ContentDetail')}}"><i class="fa fa-circle-o"></i>Content </a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share-alt" style="font-size: 4em;width:100%;"></i> <div>Channels</div>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{URL::to('Channels')}}"><i class="fa fa-circle-o"></i>Content List</a></li>
            <li><a href="{{URL::to('ChannelDetail')}}"><i class="fa fa-circle-o"></i>Channel </a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="{{URL::to('Campaigns')}}">
            <i class="fa fa-volume-up" style="font-size: 4em;width:100%;"></i> <div>Campaigns</div>
          </a>
        </li>
         <li class="treeview">
          <a href="{{URL::to('Reports')}}">
            <i class="fa fa-bar-chart" style="font-size: 4em;width:100%;"></i> <div>Reports</div>
          </a>
        </li>
      
      </ul> -->

       <ul class="sidebar-menu">
       <!--  <li class="header">MAIN NAVIGATION</li> -->
        <li class="treeview">
          <a href="/dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
         <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-dot-circle-o"></i><span>Leads</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/leads"><i class="fa fa-circle-o"></i>Lead</a></li>
            <li><a href="/LeadID"><i class="fa fa-circle-o"></i>Lead ID</a></li>
          </ul>
        </li> -->
        <li class="treeview">
          <a href="/leads">
            <i class="fa fa-fw fa-dot-circle-o"></i><span> Leads</span>
          </a>
        </li>
        <li class="treeview">
          <a href="/Content">
            <i class="fa fa-list-alt"></i> <span> Contents</span>
          </a>
        </li>
        <li class="treeview">
          <a href="/Channels">
            <i class="fa fa-share-alt"></i> <span>Channels</span>
          </a>
        </li>

         <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Content</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/Content"><i class="fa fa-circle-o"></i>Content</a></li>
            <li><a href="/ContentDetail"><i class="fa fa-circle-o"></i>Content Detail </a></li>
          </ul>
        </li> -->
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-share-alt"></i> <span>Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/Channels"><i class="fa fa-circle-o"></i>Channel</a></li>
            <li><a href="/ChannelDetail"><i class="fa fa-circle-o"></i>Channel Detail </a></li>
          </ul>
        </li> -->
         <li class="treeview">
          <a href="/Campaigns">
            <i class="fa fa-volume-up" ></i> <span>Campaigns</span>
          </a>
        </li>
         <li class="treeview">
          <a href="/Reports">
            <i class="fa fa-bar-chart" ></i> <span>Reports</span>
          </a>
        </li>
    </section>
    <!-- /.sidebar -->
  </aside>
